const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('droit', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    etat: {
      type: DataTypes.STRING(1),
      allowNull: false
    },
    libelle: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    code: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'droit',
    schema: 'public',
    timestamps: true,
    indexes: [
      {
        name: "undefined_droit_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
