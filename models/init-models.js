var DataTypes = require("sequelize").DataTypes;
var _SequelizeMeta = require("./SequelizeMeta");
var _clock = require("./clock");
var _droit = require("./droit");
var _role = require("./role");
var _role_droit = require("./role_droit");
var _utilisateur = require("./utilisateur");
var _utilisateur_role = require("./utilisateur_role");

function initModels(sequelize) {
  var SequelizeMeta = _SequelizeMeta(sequelize, DataTypes);
  var clock = _clock(sequelize, DataTypes);
  var droit = _droit(sequelize, DataTypes);
  var role = _role(sequelize, DataTypes);
  var role_droit = _role_droit(sequelize, DataTypes);
  var utilisateur = _utilisateur(sequelize, DataTypes);
  var utilisateur_role = _utilisateur_role(sequelize, DataTypes);

  role_droit.belongsTo(droit, { as: "droit", foreignKey: "droit_id"});
  droit.hasMany(role_droit, { as: "role_droits", foreignKey: "droit_id"});
  role_droit.belongsTo(role, { as: "role", foreignKey: "role_id"});
  role.hasMany(role_droit, { as: "role_droits", foreignKey: "role_id"});
  utilisateur_role.belongsTo(role, { as: "role", foreignKey: "role_id"});
  role.hasMany(utilisateur_role, { as: "utilisateur_roles", foreignKey: "role_id"});
  clock.belongsTo(utilisateur, { as: "user", foreignKey: "user_id"});
  utilisateur.hasMany(clock, { as: "clocks", foreignKey: "user_id"});
  utilisateur_role.belongsTo(utilisateur, { as: "utilisateur", foreignKey: "utilisateur_id"});
  utilisateur.hasMany(utilisateur_role, { as: "utilisateur_roles", foreignKey: "utilisateur_id"});

  return {
    SequelizeMeta,
    clock,
    droit,
    role,
    role_droit,
    utilisateur,
    utilisateur_role,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
