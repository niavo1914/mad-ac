const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('role_droit', {
    role_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'role',
        key: 'id'
      }
    },
    droit_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'droit',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'role_droit',
    schema: 'public',
    timestamps: true
  });
};
