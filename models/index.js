'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const initModels = require("./init-models").initModels;
const basename = path.basename(__filename);
const env = process.env._NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.js')[env];
const db = {};
const logger = require('../services/logger');


let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, {logging: false, ...config});
}
sequelize.options.logging = function (sql, queryObject) {
  logger.database(sql);
};


let init = initModels(sequelize);

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

//sequelize.sync({ force: true }).then(() => {
module.exports = init;
//})
