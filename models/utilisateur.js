const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('utilisateur', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    motDePasse: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    etat: {
      type: DataTypes.STRING(1),
      allowNull: true,
      defaultValue: "0"
    },
    salt: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    nom: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    prenom: {
      type: DataTypes.STRING(100),
      allowNull: true,
      unique: "utilisateur_pk"
    },
    tel: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    urlMotDePasse: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    exceptionlunch: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
    sequelize,
    tableName: 'utilisateur',
    schema: 'public',
    timestamps: true,
    indexes: [
      {
        name: "undefined_utilisateur_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "utilisateur_pk",
        unique: true,
        fields: [
          { name: "prenom" },
        ]
      },
    ]
  });
};
