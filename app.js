var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var socket_io = require('socket.io');
var session = require('express-session');
var flash = require('connect-flash');
var passport = require('passport');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');

var app = express();

// Default options applied currently. For more configuration go to express-fileupload link above
app.use(fileUpload({
  useTempFiles : true,
  tempFileDir : '/tmp/'
}));

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));

//Socket.io
var io = socket_io();
app.io = io;

var routes = require('./routes')(io);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
  secret: 'Vanilla Pay',
  resave: false,
  saveUninitialized: false,
}));
app.use(flash());


//PassPort authentification
app.use(passport.initialize());
app.use(passport.session());

//Routes
app.use('/',routes);

//load passport strategies

app.use(express.static(path.join(__dirname, 'public')));
//Imports dist from assets
app.use('/assets',express.static(path.join(__dirname, 'assets')));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  //next(createError(404));
  res.render('404')
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  if (req.app.get('env')){
    // render the error page
    res.status(err.status || 500);
    res.render('error');
  }else{
    // render the error page
    res.status(err.status || 500);
    res.render('500');
  }
});

app = require('./services/helper')(app);

module.exports = app;
