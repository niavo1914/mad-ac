require('dotenv').config();
module.exports = {
  "development": {
    "username": process.env._DB_USERNAME,
    "password": process.env._DB_PASSWORD,
    "database": process.env._DB_NAME,
    "host": process.env._DB_HOST,
    "port": process.env._DB_PORT,
    "dialect": "postgres",
    "dialectOptions": {
      "useUTC": false, //for reading from database
      "dateStrings": true,
      "typeCast": function (field, next) { // for reading from database
        if (field.type === 'DATETIME') {
          return field.string()
        }
        return next()
      },
    },
    "timezone": '+03:00',
    "logging": true,
  },
  "test": {},
  "production": {},
};
