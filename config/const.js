const CONST = {
    INACTIF: '0',
    ACTIF: '1',
    BLOQUE: '2',
    SUPPRIME: '9',
    A_IMPRIMER: '1',
    A_ENCODER: '2',
    A_VALIDER: '3',
    A_LIVRER: '4',
    LIVRE: '5',
    ERREUR_TECH_MESSAGE: "Erreur Serveur/Technique",
    ERREUR_PROBLEME_DROIT: "Vous n'avez pas le droit d'accéder à cette page",
    SUPERADMIN: "SuperAdmin",
    TYPE_LOT_COMM : '0',
    TYPE_LOT_PUB: '1',
    BUSINESS_TIMEOUT: 10000,
    BUSINESS_TIMEOUT_RECHARGEMENT_MM: 60000,
    LIMIT_FACTURE_PAIEMENT: 5,
    TYPE_TRANSACTION_HISTORIQUE: ["1", "2"]
}
module.exports = CONST;
module.exports.CONST_VUE = {
    INACTIF: CONST.INACTIF,
    ACTIF: CONST.ACTIF,
    BLOQUE: CONST.BLOQUE,
    SUPPRIME: CONST.SUPPRIME,
    A_IMPRIMER: CONST.A_IMPRIMER,
    A_ENCODER: CONST.A_ENCODER,
    A_VALIDER: CONST.A_VALIDER,
    A_LIVRER: CONST.A_LIVRER,
    LIVRE: CONST.LIVRE,
    TYPE_LOT_COMM : CONST.TYPE_LOT_COMM,
    TYPE_LOT_PUB: CONST.TYPE_LOT_PUB,
    HOST: process.env._BUSINESS_HOST
}

module.exports.Etat = {
    INACTIF: '0',
    ACTIF: '1',
    BLOQUE: '2',
    UTILISATEUR_ETAT_INITIAL: '3',
    SUPPRIME: '9',
    ANNULE: '9',
    A_IMPRIMER: '1',
    A_ENCODER: '2',
    A_VALIDER: '3',
    A_LIVRER: '4',
    RATTACHE: '1',
    DETACHE: '2'
}