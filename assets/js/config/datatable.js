const configDT = (order, columnDefs, boutons) => {
    return {
        dom: 'Bfrtip<"clear">l',
        info: 1,
        order: order,//order: [[ 1, 'asc' ]],
        columnDefs: columnDefs,//columnDefs: [{ orderable: !1, targets: 4 },],
        "language": {
            "sProcessing": "Traitement en cours ...",
            "sLengthMenu": "Afficher _MENU_",
            "sZeroRecords": "Aucun résultat trouvé",
            "sEmptyTable": "Aucune donnée disponible",
            "sInfo": "lignes sur _TOTAL_",
            "sInfoEmpty": "Aucune ligne affichée",
            "sInfoFiltered": "(Filtrer un maximum de _MAX_)",
            "sInfoPostFix": "",
            "sSearch": "Chercher:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Chargement...",
            /*"oPaginate": {
                "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
            },*/
            "oAria": {
                "sSortAscending": ": Trier par ordre croissant",
                "sSortDescending": ": Trier par ordre décroissant"
            }
        },
        buttons: boutons,
        initComplete: function () {
            // Remove the default search input added by DataTables
            $('.dataTables_filter').remove();
        }
    }
}

const countRowFilter = function (datatable, columnId, filtre) {
    let count = datatable.column(columnId).data().filter(function (value) {
        let etat = $(value)[0].innerHTML;
        return (etat == filtre)
    }).count();

    return count;
}

function formatString(text, separator, groupBy) {

    if (!text) return "";

    let ret = "";
    for (let i = text.length; i > 0; i = i - groupBy) {
        let j = (i - groupBy < 0) ? 0 : i - groupBy;
        groupBy = (i - groupBy < 0) ? i : groupBy;

        let s = text.substr(j, groupBy)
        const sep = (ret == "") ? "" : separator;
        ret = s + sep + ret;
    }
    return ret
}