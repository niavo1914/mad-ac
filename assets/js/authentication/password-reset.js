"use strict";
var KTPasswordResetGeneral = (function () {
    var t, e, i, j;
    return {
        init: function () {
            (t = document.querySelector("#kt_password_reset_form")),
                (e = document.querySelector("#kt_password_reset_submit")),
                (j = document.querySelector("#kt_password_reset_cancel")),
                (i = FormValidation.formValidation(t, {
                    fields: {
                        email: { validators: { notEmpty: { message: "Champ obligatoire" }, regexp:{ regexp: "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$",message: "Veuillez saisir une adresse email valide"} } },
                    },
                    plugins: { trigger: new FormValidation.plugins.Trigger(), bootstrap: new FormValidation.plugins.Bootstrap5({ rowSelector: ".fv-row", eleInvalidClass: "", eleValidClass: "" }) },
                })),
                e.addEventListener("click", function (o) {
                    o.preventDefault(),
                        i.validate().then(function (i) {
                            "Valid" == i
                                ? (e.setAttribute("data-kt-indicator", "on"),
                                        (e.disabled = !0),
                                        t.submit()
                                )
                                : true
                        });
                }),
                j.addEventListener("click", function (l) {
                    l.preventDefault(),
                        Swal.fire({
                            text: "Êtes-vous sûr de vouloir annuler?",
                            icon: "warning",
                            showCancelButton: !0,
                            buttonsStyling: !1,
                            confirmButtonText: "Oui",
                            cancelButtonText: "Non",
                            customClass: {confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light"},
                        }).then(function (l) {
                            l.value
                                ? (t.reset(), i.hide())
                                : "cancel" === l.dismiss
                        });
                })
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTPasswordResetGeneral.init();
});
