"use strict";
var KTUsersUpdateRole = (function () {
    const update_role_div = document.getElementById("kt_user_view_role_tab");

    var submit_update_role = (button_submit)=>{
        try{
            let role_user = []
            //check if all checkboxes are unchecked

            if (!all_role_checkboxes_are_checked(role_user)) throw new Error("Veuillez séléctionner au moins un rôle")

            const e = document.getElementById("kt_user_view_role_tab"),
                _id = e.querySelector('input[name="user_id"]').value
            //submit form
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: "/utilisateur/update-role/" + _id, //à dynamiser
                    type: 'post',
                    dataType: "jsonp",
                    data: {roles: JSON.stringify(role_user)},
                    statusCode: {
                        200: function (response) {
                            response = JSON.parse(response.responseText);
                            switch (response.status) {
                                case 200 :
                                    resolve(response.status)
                                    break;
                                case 500 :
                                    reject(response.msg)
                                    break;
                            }
                        },
                        500: function (err) {
                            reject(err)
                        }
                    }
                })
            })
        }catch (e) {
            Swal.fire({
                text: e.message,
                icon: "error",
                buttonsStyling: !1,
                confirmButtonText: "OK",
                customClass: {confirmButton: "btn btn-primary"},
            })
        }
    }

    var all_role_checkboxes_are_checked = (role_user) => {
        const e = document.getElementById("kt_user_view_role_tab"),
            textinputs = e.querySelectorAll('.check_droit');

        var empty = [].filter.call(textinputs, function (el) {
            role_user.push(
                {
                    id: el.value, associe: el.checked
                }
            )
            return !el.checked
        });

        if (textinputs.length == empty.length) {
            return false;
        }
        return true;
    }
    const onChangeCheckBox = ()=>{
        document.querySelectorAll('.check_droit').forEach((e)=>{
            e.addEventListener("change",(t)=>{
                t.preventDefault()
                if (e.checked) //increment libellé nbre role associé
                {
                    document.getElementById("nbrRoleAssocie").textContent = Number(document.getElementById("nbrRoleAssocie").innerText)+1
                }else{
                    document.getElementById("nbrRoleAssocie").textContent = Number(document.getElementById("nbrRoleAssocie").innerText)-1
                }
               // console.log("onchange checkbox "+e.checked)
            })
        })
    }

    return {
        init: function () {
            (() => {
              const i = update_role_div.querySelector('button[name="save_modif_role"]')
                  i.addEventListener("click",function (m) {
                    m.preventDefault();
                      Swal.fire({
                          html: 'Êtes-vous sûr de vouloir attacher les rôles séléctionnés?',
                          icon: "warning",
                          showCancelButton: !0,
                          buttonsStyling: !1,
                          showLoaderOnConfirm: true,
                          allowOutsideClick: () => !Swal.isLoading(),
                          preConfirm: async () => {
                              try {
                                  const ret = await submit_update_role()
                                  return true
                              } catch (e) {
                                  Swal.fire({
                                      title: e?e:`Les rôles n'ont pas été attachés.`,
                                      text: "",
                                      icon: "error",
                                      buttonsStyling: !1,
                                      confirmButtonText: "Ok",
                                      customClass: {confirmButton: "btn fw-bold btn-primary"}
                                  });
                              }
                          },
                          confirmButtonText: "Enregistrer",
                          cancelButtonText: "Annuler",
                          customClass: {
                              confirmButton: "btn fw-bold btn-danger",
                              cancelButton: "btn fw-bold btn-active-light-primary"
                          },
                      }).then(function (e) {
                          e.value
                              ? Swal.fire({
                                  html: 'Les rôles séléctionnés ont bien été attachés.',
                                  icon: "success",
                                  buttonsStyling: !1,
                                  confirmButtonText: "Ok",
                                  customClass: {confirmButton: "btn fw-bold btn-primary"}
                              }).then(function () {
                                  //t.row($(n)).remove().draw();
                                  window.location.replace(window.location.pathname);
                              })
                              : "cancel" === e.dismiss &&
                              Swal.fire({
                                  html: `Les rôles n'ont pas été attachés.`,
                                  icon: "error",
                                  buttonsStyling: !1,
                                  confirmButtonText: "Ok",
                                  customClass: {confirmButton: "btn fw-bold btn-primary"}
                              });
                      });
                });
            })(),
                (() => {
                    onChangeCheckBox()
                })();
        },
    };
})();

KTUtil.onDOMContentLoaded(function () {
    KTUsersUpdateRole.init();
});
