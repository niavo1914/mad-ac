"use strict";
var KTCustomersList = (function () {
    let t,
        e,
        o,
        n,
        event_update_row =()=>{
            $('#kt_body').on('click', '[data-kt-customer-table-filter="update_row"]', function (e) {
                e.preventDefault();
                const n = e.target.closest("tr"),
                    _id = n.querySelectorAll("td")[0].innerText,
                    nom = n.querySelectorAll("td")[1].innerText,
                    prenom = n.querySelectorAll("td")[2].innerText,
                    email = n.querySelectorAll("td")[3].innerText,
                    tel = n.querySelectorAll("td")[4].innerText,
                    role = n.querySelectorAll("td")[6].innerText;
                $('#update_id').val(_id)
                $('#update-name').val(nom)
                $('#update-prenom').val(prenom)
                $('#update-email').val(email)
                $('#update-phone').val(tel);
                $('#update-role').val(role);

            })
        };
    return {
        init: function () {
            (n = document.querySelector("#kt_user_table")) &&
            (n.querySelectorAll("tbody tr").forEach((t) => {
                    const e = t.querySelectorAll("td")
                    /*o = moment(e[5].innerHTML, "DD MMM YYYY, LT").format();
                e[5].setAttribute("data-order", o);*/
                }),
                    (t = $(n).DataTable(configDT(
                        [[1, 'asc']],
                        [
                            {orderable: false, targets: 7}
                        ],
                        []
                    ))),
                    document.querySelector('[data-kt-user-table-filter="search"]').addEventListener("keyup", function (e) {
                        t.search(e.target.value).draw();
                    })
            ),
                event_update_row()
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTCustomersList.init();
});
