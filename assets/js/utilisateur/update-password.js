"use strict";
var KTUsersUpdatePassword = (function () {
    const t = document.getElementById("kt_modal_update_password"),
        e = t.querySelector("#kt_modal_update_password_form"),
        n = new bootstrap.Modal(t);
    let o, k;
    const validatePassword = function () {
        return 100 === k.getScore();
    };
    const submit_update_form = (modalElt, submitButt) => {
        try {
            const _id = modalElt.querySelector('input[name="id_user"]').value
            const currenPass = modalElt.querySelector('input[name="current_password"]').value
            const newPass = modalElt.querySelector('input[name="new_password"]').value
            const confirmPass = modalElt.querySelector('input[name="confirm_password"]').value
            $.ajax({
                url: "/utilisateur/changePassword/" + _id, //à dynamiser
                type: 'post',
                dataType: "jsonp",
                data: {currentPass: currenPass, newPass: newPass, confirmPass:confirmPass},
                statusCode: {
                    200: function (response) {
                        response = JSON.parse(response.responseText);
                        switch (response.status) {
                            case 200 :
                                Swal.fire({
                                        text: response.msg,
                                        icon: "success",
                                        buttonsStyling: !1,
                                        confirmButtonText: "OK",
                                        customClass: {confirmButton: "btn btn-primary"}
                                    }
                                ).then(
                                    function (t) {
                                        //t.isConfirmed && n.hide()
                                        //const redirect_to = (LET.mode) === 'liste' ? "/role" : "/role/"+ LET.roleId
                                        submitButt.removeAttribute("data-kt-indicator"),
                                            (submitButt.disabled = !1)
                                        window.location.replace(window.location.pathname);
                                    })
                                break;
                            case 500 :
                                Swal.fire({
                                    text: response.msg,
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "OK",
                                    customClass: {confirmButton: "btn btn-primary"},
                                })
                                submitButt.removeAttribute("data-kt-indicator"),
                                    (submitButt.disabled = !1)
                                break;
                        }
                    },
                    500: function (err) {
                        /*Swal.fire({
                            text: err.responseText,
                            icon: "error",
                            buttonsStyling: !1,
                            confirmButtonText: "OK",
                            customClass: { confirmButton: "btn btn-primary" },
                        })*/
                    }
                }
            })
        } catch (e) {
            console.log(e)
        } finally {

        }
    }

    return {
        init: function () {
            (() => {
                (k = KTPasswordMeter.getInstance(e.querySelector('[data-kt-password-meter="true"]'))),
                    o = FormValidation.formValidation(e, {
                    fields: {
                        current_password: { validators: { notEmpty: { message: "Champ obligatoire" } } },
                        new_password: {
                            validators: {
                                notEmpty: { message: "Champ obligatoire" },
                                callback: {
                                    message: "Veuillez saisir un mot de passe valide ",
                                    callback: function (t) {
                                        if (t.value.length > 0) return validatePassword();
                                    },
                                },
                            },
                        },
                        confirm_password: {
                            validators: {
                                notEmpty: { message: "Champ obligatoire" },
                                identical: {
                                    compare: function () {
                                        return e.querySelector('[name="new_password"]').value;
                                    },
                                    message: "Le mot de passe et sa confirmation ne sont pas identiques",
                                },
                            },
                        },
                    },
                    plugins: { trigger: new FormValidation.plugins.Trigger(), bootstrap: new FormValidation.plugins.Bootstrap5({ rowSelector: ".fv-row", eleInvalidClass: "", eleValidClass: "" }) },
                });
                t.querySelector('[data-kt-users-modal-action="close"]').addEventListener("click", (t) => {
                    t.preventDefault(),
                        Swal.fire({
                            text: "Êtes-vous sûr de vouloir annuler?",
                            icon: "warning",
                            showCancelButton: !0,
                            buttonsStyling: !1,
                            confirmButtonText: "Oui",
                            cancelButtonText: "Non",
                            customClass: { confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light" },
                        }).then(function (t) {
                            t.value
                                ? (e.reset(), n.hide())
                                : "cancel" === t.dismiss
                        });
                }),
                    t.querySelector('[data-kt-users-modal-action="cancel"]').addEventListener("click", (t) => {
                        t.preventDefault(),
                            Swal.fire({
                                text: "Êtes-vous sûr de vouloir annuler?",
                                icon: "warning",
                                showCancelButton: !0,
                                buttonsStyling: !1,
                                confirmButtonText: "Oui",
                                cancelButtonText: "Non",
                                customClass: { confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light" },
                            }).then(function (t) {
                                t.value
                                    ? (e.reset(), n.hide())
                                    : "cancel" === t.dismiss
                            });
                    });
                const a = t.querySelector('[data-kt-users-modal-action="submit"]');
                a.addEventListener("click", function (t) {
                    t.preventDefault(),
                    o &&
                    o.revalidateField("new_password"),
                    o.validate().then(function (t) {
                        console.log("validated!"),
                            "Valid" == t
                                ?
                                (a.setAttribute("data-kt-indicator", "on"),
                                        (a.disabled = !0),
                                        submit_update_form(e,a)
                                )
                                : true
                    });
                });
                e.querySelector('input[name="new_password"]').addEventListener("input", function () {
                    this.value.length > 0 && o.updateFieldStatus("new_password", "NotValidated");
                });
            })();
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTUsersUpdatePassword.init();
});
