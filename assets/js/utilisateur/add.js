"use strict";

var KTModalCustomersAdd = (function () {
    var table, t, e, o, n, r, i, l;
    const submit_update_form = (modalElt) => {

        const _name = modalElt.querySelector('input[name="new-name"]').value
        const _firstName = modalElt.querySelector('input[name="new-prenom"]').value
        const _email = modalElt.querySelector('input[name="new-email"]').value
        const _phone = modalElt.querySelector('input[name="new-phone"]').value

        const Stop_loading_button = (modalElt) => {
            const i = modalElt.querySelector('#kt_modal_add_customer_submit');
            i.removeAttribute("data-kt-indicator"),
                (i.disabled = !1)
        }

        $.ajax({
            url: "/utilisateur/create/", //à dynamiser
            type: 'post',
            dataType: "jsonp",
            data: {nom: _name, firstName: _firstName, email: _email, phone: _phone},
            statusCode: {
                200: function (response) {
                    response = JSON.parse(response.responseText);
                    switch (response.CodeRetour) {
                        case 200 :
                            Stop_loading_button(modalElt)
                            Swal.fire({
                                    text: response.DetailRetour,
                                    icon: "success",
                                    buttonsStyling: !1,
                                    confirmButtonText: "OK",
                                    customClass: {confirmButton: "btn btn-primary"}
                                }
                            ).then(
                                function (t) {
                                    //t.isConfirmed && n.hide()
                                    //const redirect_to = (LET.mode) === 'liste' ? "/role" : "/role/"+ LET.roleId
                                    window.location.replace("/utilisateur");
                                })
                            break;
                        default :
                            Stop_loading_button(modalElt)
                            Swal.fire({
                                text: response.DetailRetour,
                                icon: "error",
                                buttonsStyling: !1,
                                confirmButtonText: "OK",
                                customClass: {confirmButton: "btn btn-primary"},
                            })
                    }
                },
                500: function (err) {
                    Stop_loading_button(modalElt)
                    /*Swal.fire({
                        text: err.responseText,
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "OK",
                        customClass: { confirmButton: "btn btn-primary" },
                    })*/
                }
            }
        })

    };
    const revalidate_champTel = (formElt, formValidation) => {
        $(formElt.querySelector('[name="new-phone"]')).on("keyup", function () {
            formValidation.revalidateField("new-phone");
        })
    }
    const onHideModal = (formValidation)=>{
        $( "#kt_modal_add_customer" ).on('hide.bs.modal', function(t){
            formValidation.resetForm()
        });
    }
    return {
        init: function () {
            (l = document.querySelector("#kt_agent_table")) &&
            (table = $(l).DataTable({
                info: 1,
                order: [[1, 'asc']],
                columnDefs: [
                    {orderable: !1, targets: 3},
                    {orderable: !1, targets: 5},
                ],
                "language": {
                    "sProcessing": "Traitement en cours ...",
                    "sLengthMenu": "Afficher _MENU_",
                    "sZeroRecords": "Aucun résultat trouvé",
                    "sEmptyTable": "Aucune donnée disponible",
                    "sInfo": "lignes sur _TOTAL_",
                    "sInfoEmpty": "Aucune ligne affichée",
                    "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
                    "sInfoPostFix": "",
                    "sSearch": "Chercher:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Chargement...",
                    /*"oPaginate": {
                        "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
                    },*/
                    "oAria": {
                        "sSortAscending": ": Trier par ordre croissant",
                        "sSortDescending": ": Trier par ordre décroissant"
                    }
                }
            })),
                document.querySelector('[data-kt-user-table-filter="search"]').addEventListener("keyup", function (e) {
                    table.search(e.target.value).draw();
                }),
                (i = new bootstrap.Modal(document.querySelector("#kt_modal_add_customer"))),
                (r = document.querySelector("#kt_modal_add_customer_form")),
                (t = r.querySelector("#kt_modal_add_customer_submit")),
                (e = r.querySelector("#kt_modal_add_customer_cancel")),
                (o = r.querySelector("#kt_modal_add_customer_close")),
                (n = FormValidation.formValidation(r, {
                    fields: {
                        "new-name": {validators: {notEmpty: {message: "Champ obligatoire"}}},
                        "new-prenom": {validators: {notEmpty: {message: "Champ obligatoire"}}}
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: ".fv-row",
                            eleInvalidClass: "",
                            eleValidClass: ""
                        })
                    },
                })),
                onHideModal(n),
                revalidate_champTel(r, n),
                t.addEventListener("click", function (e) {
                    e.preventDefault(n),
                    n &&
                    n.validate().then(function (e) {
                        console.log("validated!"),
                            "Valid" == e
                                ? (t.setAttribute("data-kt-indicator", "on"),
                                        (t.disabled = !0),
                                        submit_update_form(r)
                                )
                                : null;
                    });
                }),
                e.addEventListener("click", function (t) {
                    t.preventDefault(),
                        i.hide()
                }),
                o.addEventListener("click", function (t) {
                    t.preventDefault(),
                        i.hide()
                });
        },
    };
})();

KTUtil.onDOMContentLoaded(function () {
    KTModalCustomersAdd.init();
});
