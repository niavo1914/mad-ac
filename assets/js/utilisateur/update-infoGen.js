"use strict";
var KTUsersUpdateInfo = (function () {
    const t = document.getElementById("kt_modal_update_details"),
        e = t.querySelector("#kt_modal_update_user_form"),
        n = new bootstrap.Modal(t);
    var l;

    const submit_update_form = (modalElt, submitButt) => {
        try {

            const _id = modalElt.querySelector('input[name="update_id"]').value
            const _name = modalElt.querySelector('input[name="update-name"]').value
            const _firstName = modalElt.querySelector('input[name="update-prenom"]').value
            const _phone = modalElt.querySelector('input[name="update-phone"]').value
            const _mail = modalElt.querySelector('input[name="update-email"]').value
            const _role = $('#update-role').val();

            $.ajax({
                url: "/utilisateur/update/" + _id, //à dynamiser
                type: 'post',
                dataType: "jsonp",
                data: {nom: _name, prenom: _firstName, phone: _phone, mail:_mail, role:_role},
                statusCode: {
                    200: function (response) {
                        submitButt.removeAttribute("data-kt-indicator"),


                            (submitButt.disabled = !1)
                        response = JSON.parse(response.responseText);
                        switch (response.CodeRetour) {
                            case 200 :
                                Swal.fire({
                                        text: response.DetailRetour,
                                        icon: "success",
                                        buttonsStyling: !1,
                                        confirmButtonText: "OK",
                                        customClass: {confirmButton: "btn btn-primary"}
                                    }
                                ).then(
                                    function (t) {
                                        //t.isConfirmed && n.hide()
                                        //const redirect_to = (LET.mode) === 'liste' ? "/role" : "/role/"+ LET.roleId
                                        window.location.replace(window.location.pathname);
                                    })
                                break;
                            default :
                                Swal.fire({
                                    text: response.DetailRetour,
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "OK",
                                    customClass: {confirmButton: "btn btn-primary"},
                                })
                        }
                    },
                    500: function (err) {
                        submitButt.removeAttribute("data-kt-indicator"),
                            (submitButt.disabled = !1)
                        /*Swal.fire({
                            text: err.responseText,
                            icon: "error",
                            buttonsStyling: !1,
                            confirmButtonText: "OK",
                            customClass: { confirmButton: "btn btn-primary" },
                        })*/
                    }
                }
            })
        } catch (e) {
            submitButt.removeAttribute("data-kt-indicator"),
                (submitButt.disabled = !1)
            console.log(e)
        }
    }
    const onHideModal = (formValidation)=>{
        $( "#kt_modal_update_details" ).on('hide.bs.modal', function(t){
            formValidation.resetForm()
        });
    }
    return {
        init: function () {
            (() => {
                (l = FormValidation.formValidation(e, {
                    fields: {
                        "update-name": {validators: {notEmpty: {message: "Champ obligatoire"}}},
                        "update-prenom": {validators: {notEmpty: {message: "Champ obligatoire"}}}
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: ".fv-row",
                            eleInvalidClass: "",
                            eleValidClass: ""
                        })
                    },
                })),
                    onHideModal(l),
                    t.querySelector('input[name="update-phone"]').addEventListener("keyup",(t)=>{
                        l.revalidateField("update-phone");
                    }),
                    t.querySelector('[data-kt-users-modal-action="close"]').addEventListener("click", (t) => {
                        t.preventDefault(),
                            Swal.fire({
                                text: "Êtes-vous sûr de vouloir annuler?",
                                icon: "warning",
                                showCancelButton: !0,
                                buttonsStyling: !1,
                                confirmButtonText: "Oui",
                                cancelButtonText: "Non",
                                customClass: {confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light"},
                            }).then(function (t) {
                                t.value
                                    ? (console.log("hide closer"),e.reset(), n.hide())
                                    : "cancel" === t.dismiss, n.hide()
                            });
                    }),
                    t.querySelector('[data-kt-users-modal-action="cancel"]').addEventListener("click", (t) => {
                        t.preventDefault(),
                            Swal.fire({
                                text: "Êtes-vous sûr de vouloir annuler?",
                                icon: "warning",
                                showCancelButton: !0,
                                buttonsStyling: !1,
                                confirmButtonText: "Oui",
                                cancelButtonText: "Non",
                                customClass: {confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light"},
                            }).then(function (t) {
                                t.value
                                    ? (e.reset(), n.hide())
                                    : "cancel" === t.dismiss
                            });
                    });
                const o = t.querySelector('[data-kt-users-modal-action="submit"]');
                o.addEventListener("click", function (t) {
                    t.preventDefault(),
                    l &&
                    l.validate().then(function (m) {
                        console.log("validated!"),
                            "Valid" == m
                                ? (o.setAttribute("data-kt-indicator", "on"),
                                    (o.disabled = !0),
                                    submit_update_form(e, o)
                                )
                                : null;
                    })

                })
            })(),
                (() => {

                })();
        },
    };
})();

KTUtil.onDOMContentLoaded(function () {
    KTUsersUpdateInfo.init();
});
