let changerEtat;
$(document).ready(function () {
    const postData = (id, etat) => {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: "/utilisateur/changeEtat/"+id, //à dynamiser
                type: 'post',
                dataType : "jsonp",
                data : {etat:etat},
                statusCode: {
                    200: function (response) {
                        response = JSON.parse(response.responseText)
                        switch (response.status) {
                            case 200:
                                resolve("ok")
                                break;
                            case 500:
                                reject("ko")
                                break
                        }
                    },
                    500: function (response) {
                        reject("ko")
                    }
                }
            })
        })
    }
    changerEtat = (_libelle, _etat, _idUser) => {

        const verb = _etat === 1 ? 'réactiver' : 'Désactiver';
        const Verb = _etat === 1 ? 'Réactiver' : 'Désactiver';
        const Noun = _etat === 1 ? 'Réactivation' : 'Désactivation';
        const cong = _etat === 1 ? 'réactivé' : 'Désactivé';

        Swal.fire({
            title: Noun,
            html: `Êtes-vous sûr de vouloir ${verb} <b>${_libelle}</b> ?`,
            icon: "warning",
            showCancelButton: !0,
            buttonsStyling: !1,
            showLoaderOnConfirm: true,
            allowOutsideClick: () => !Swal.isLoading(),
            preConfirm: async () => {
                try {
                    const ret = await postData(_idUser, _etat)
                    return true
                } catch (e) {
                    Swal.fire({
                        html: `<b>${_libelle}</b> n'a pas été ${cong}.`,
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok",
                        customClass: {confirmButton: "btn fw-bold btn-primary"}
                    });
                }
            },
            confirmButtonText: Verb,
            cancelButtonText: "Annuler",
            customClass: {
                confirmButton: "btn fw-bold btn-danger",
                cancelButton: "btn fw-bold btn-active-light-primary"
            },
        }).then(function (e) {
            e.value
                ? Swal.fire({
                    html: `Vous avez ${cong} <b>${_libelle}</b>.`,
                    icon: "success",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok",
                    customClass: {confirmButton: "btn fw-bold btn-primary"}
                }).then(function () {
                    //t.row($(n)).remove().draw();
                    window.location.replace(window.location.pathname);
                })
                : "cancel" === e.dismiss &&
                Swal.fire({
                    html: `<b>${_libelle}</b> n'a pas été ${cong}.`,
                    icon: "error",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok",
                    customClass: {confirmButton: "btn fw-bold btn-primary"}
                });
        });

    };

    reinitiate = (nom, id) => {
        Swal.fire({
            title: nom,
            html: `Êtes-vous sûr de vouloir réinitialiser son mot de passe ?`,
            icon: "warning",
            showCancelButton: !0,
            buttonsStyling: !1,
            showLoaderOnConfirm: true,
            allowOutsideClick: () => !Swal.isLoading(),
            confirmButtonText: "Oui",
            cancelButtonText: "Annuler",
            customClass: {
                confirmButton: "btn fw-bold btn-danger",
                cancelButton: "btn fw-bold btn-active-light-primary"
            },
        }).then(result=> {
           if(result.isConfirmed) {
               var progress = Swal.fire({
                   title: "Réinitialisation...",
                   allowOutsideClick : false,
                   didOpen: () => {
                       Swal.showLoading();
                   },
               });
               $.ajax({
                   url : '/utilisateur/resetPassword/'+id,
                   method : 'get',
                   statusCode : {
                     200 : function(response) {
                         result.dismiss;
                         progress.close();
                         Swal.fire({
                             title : "Mot de passe réinitialisé : "+response,
                             icon: "success",
                         })
                     }  ,
                       500  : function(response) {
                           progress.close();
                           Swal.fire({
                               title : response.responseText,
                               icon: "error",
                           })
                       }
                   }
               })
           }
        });
    }
})
