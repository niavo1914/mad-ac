let _envoi_email_validation;
$(document).ready(function(){
    _envoi_email_validation = (_userEmail,_idUser)=>{
        Swal.fire({
            title: 'Veuillez Patienter ...',
            text: 'Envoi email de validation en cours',
            icon: 'info',
            timerProgressBar: true,
            didOpen: () => {
                Swal.showLoading()
                $.ajax({
                    url:"/responsable/sendMailValidation",
                    type:'post',
                    dataType:'jsonp',
                    data:{email:_userEmail},
                    statusCode:{
                        200:(response)=>{
                            response = JSON.parse(response.responseText);
                            Swal.close()

                            switch (response.status) {
                                case 200:
                                    Swal.fire({
                                        text: "Email envoié",
                                        icon: "success",
                                        buttonsStyling: !1,
                                        confirmButtonText: "Ok",
                                        customClass: {confirmButton: "btn btn-primary"}
                                    })
                                    break;
                                case 500:
                                    Swal.fire({
                                        text: response.msg,
                                        icon: "error",
                                        buttonsStyling: !1,
                                        confirmButtonText: "Ok",
                                        customClass: {confirmButton: "btn btn-primary"}
                                    })
                                    break;
                            }
                        }
                    }
                })
            },
            willClose: () => {
            }
        }).then((result) => {
            /* Read more about handling dismissals below */

        })
    }
})