"use strict";

var KTModalCustomersAdd = (function () {
    var t, e, o, n, r, i, l;
    return {
        init: function () {
            (l = document.querySelector("#kt_user_table")) &&
            (t = $(l).DataTable({
                info: !1,
                order: [[0, 'asc']],
                columnDefs: [
                    {orderable: !1, targets: 3},
                    {orderable: !1, targets: 5},
                ],
            })),
                document.querySelector('[data-kt-user-table-filter="search"]').addEventListener("keyup", function (e) {
                    t.search(e.target.value).draw();
                }),
                (i = new bootstrap.Modal(document.querySelector("#kt_modal_add_customer"))),
                (r = document.querySelector("#kt_modal_add_customer_form")),
                (t = r.querySelector("#kt_modal_add_customer_submit")),
                (e = r.querySelector("#kt_modal_add_customer_cancel")),
                (o = r.querySelector("#kt_modal_add_customer_close")),
                (n = FormValidation.formValidation(r, {
                    fields: {
                        "new-name": {validators: {notEmpty: {message: "Nom est requis"}}},
                        "new-email": {
                            validators: {
                                notEmpty: {message: "Email est requis"},
                                regexp: {
                                    regexp: "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$",
                                    message: "Email format non accépté"
                                }
                            }
                        },
                        "new-phone": {
                            validators: {
                                notEmpty: {message: "Téléphone est requis"},
                                regexp: {
                                    regexp: "[(+261)] [3][2|3|4|9] [0-9]{2} [0-9]{3} [0-9]{2}$",
                                    message: "Numéro de téléphone non accépté"
                                }
                            }
                        }
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: ".fv-row",
                            eleInvalidClass: "",
                            eleValidClass: ""
                        })
                    },
                })),
                $(r.querySelector('[name="country"]')).on("change", function () {
                    n.revalidateField("country");
                }),
                t.addEventListener("click", function (e) {
                    e.preventDefault(),
                    n &&
                    n.validate().then(function (e) {
                        console.log("validated"),
                            "Valid" == e
                                ? (t.setAttribute("data-kt-indicator", "on"),
                                    (t.disabled = !0),
                                    setTimeout(function () {
                                        submit_update_form(r);
                                    }, 2e3))
                                : Swal.fire({
                                    text: "Désolé, il semble que des erreurs ont été détectées, veuillez réessayer.",
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok" ,
                                    customClass: {confirmButton: "btn btn-primary"},
                                });
                    });
                }),
                e.addEventListener("click", function (t) {
                    t.preventDefault(),
                        Swal.fire({
                            text: "Êtes-vous sûr de vouloir annuler?",
                            icon: "warning",
                            showCancelButton: !0,
                            buttonsStyling: !1,
                            confirmButtonText: "Oui",
                            cancelButtonText: "Non",
                            customClass: {confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light"},
                        }).then(function (t) {
                            t.value
                                ? (r.reset(), i.hide())
                                : "cancel" === t.dismiss
                        });
                }),
                o.addEventListener("click", function (t) {
                    t.preventDefault(),
                        Swal.fire({
                            text: "Êtes-vous sûr de vouloir annuler?",
                            icon: "warning",
                            showCancelButton: !0,
                            buttonsStyling: !1,
                            confirmButtonText: "Oui",
                            cancelButtonText: "Non",
                            customClass: {confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light"},
                        }).then(function (t) {
                            t.value
                                ? (r.reset(), i.hide())
                                : "cancel" === t.dismiss
                        });
                });
        },
    };
})();

var submit_update_form = (modalElt) => {

        const _name = modalElt.querySelector('input[name="new-name"]').value
        const _email = modalElt.querySelector('input[name="new-email"]').value
        const _phone = modalElt.querySelector('input[name="new-phone"]').value

        const Stop_loading_button = (modalElt)=>{
            const i = modalElt.querySelector('#kt_modal_add_customer_submit');
            i.removeAttribute("data-kt-indicator"),
                (i.disabled = !1)
        }

        $.ajax({
            url: "/utilisateur/create/", //à dynamiser
            type: 'post',
            dataType: "jsonp",
            data: {nom: _name, email: _email, phone: _phone},
            statusCode: {
                200: function (response) {
                    response = JSON.parse(response.responseText);
                    switch (response.status) {
                        case 200 :
                            Stop_loading_button(modalElt)
                            Swal.fire({
                                    text: response.msg,
                                    icon: "success",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok",
                                    customClass: {confirmButton: "btn btn-primary"}
                                }
                            ).then(
                                function (t) {
                                    //t.isConfirmed && n.hide()
                                    //const redirect_to = (LET.mode) === 'liste' ? "/role" : "/role/"+ LET.roleId
                                    window.location.replace("/utilisateur");
                                })
                            break;
                        case 500 :
                            Stop_loading_button(modalElt)
                            Swal.fire({
                                text: response.msg,
                                icon: "error",
                                buttonsStyling: !1,
                                confirmButtonText: "Ok",
                                customClass: {confirmButton: "btn btn-primary"},
                            })
                    }
                },
                500: function (err) {
                    Stop_loading_button(modalElt)
                    /*Swal.fire({
                        text: err.responseText,
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "OK",
                        customClass: { confirmButton: "btn btn-primary" },
                    })*/
                }
            }
        })

}

KTUtil.onDOMContentLoaded(function () {
    KTModalCustomersAdd.init();
});
