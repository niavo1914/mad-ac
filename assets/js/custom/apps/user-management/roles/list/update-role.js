"use strict";
var KTUsersUpdatePermissions = (function () {
    const _modal_update_container = document.getElementById("kt_modal_update_role"),
        _modal_update_form = _modal_update_container.querySelector("#kt_modal_update_role_form"),
        _modal_update_object = new bootstrap.Modal(_modal_update_container);

    //variable constante
    let CONST = {
        roleId_ifAdd : 'NEW',
        title_update_modal: 'Edition - Rôle',
        title_add_modal: 'Nouveau - Rôle'
    }
    //variable dynamique
    let LET = {
        roleId:null,
        mode:null // liste-old.ejs ou detail.ejs
    }

    return {
        init: function () {
            (() => {
                const r = document.getElementById("kt_body");
                if (r){
                    r.querySelectorAll('[data-kt-role-list-filter="update_row"]').forEach(r => {
                        r.addEventListener("click", function (r) {
                            r.preventDefault();
                            LET.roleId = r.target.dataset['roleId'];
                            LET.mode = r.target.dataset['updateMode']

                            //Recuperer info role (libelle et liste droits)
                            getRoleData(LET, CONST, _modal_update_container);
                        })
                    });
                    // only for liste-old.ejs
                    const s = r.querySelector('[data-kt-role-list-filter="add_row"]');
                    s && s.addEventListener('click',(r)=>{
                        r.preventDefault();

                        LET.roleId = CONST.roleId_ifAdd
                        LET.mode = r.target.dataset['updateMode']

                        getRoleData(LET, CONST, _modal_update_container);

                    })
                }
                //todo on shown modal update role
                /*$('#kt_modal_update_role').on('shown.bs.modal', function (e) {
                    console.log("Malala2");
                    //get data-id attribute of the clicked element
                    var roleId = $(e.relatedTarget).data('role-id');

                    //populate the textbox
                        $(e.currentTarget).find('input[name="role_name"]').val(roleId);

                })*/
            })(),
                (() => {
                    var o = FormValidation.formValidation(_modal_update_form, {
                        fields: {role_name: {validators: {notEmpty: {message: "Champ obligatoire"}}}},
                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            bootstrap: new FormValidation.plugins.Bootstrap5({
                                rowSelector: ".fv-row",
                                eleInvalidClass: "",
                                eleValidClass: ""
                            })
                        },
                    });
                    _modal_update_container.querySelectorAll('[data-kt-roles-modal-action="close"]').forEach((e) => {
                        e.addEventListener("click", (t) => {
                            t.preventDefault(),
                                Swal.fire({
                                    text: "Êtes-vous sûr de vouloir fermer?",
                                    icon: "warning",
                                    showCancelButton: !0,
                                    buttonsStyling: !1,
                                    confirmButtonText: "Oui",
                                    cancelButtonText: "Non",
                                    customClass: {
                                        confirmButton: "btn btn-primary",
                                        cancelButton: "btn btn-active-light"
                                    },
                                }).then(function (t) {
                                    t.value && _modal_update_object.hide();
                                });
                        })
                    }),
                        _modal_update_container.querySelectorAll('[data-kt-roles-modal-action="cancel"]').forEach((f)=>{
                            f.addEventListener("click", (t) => {
                                t.preventDefault(),
                                    Swal.fire({
                                        text: "Êtes-vous sûr de vouloir annuler?",
                                        icon: "warning",
                                        showCancelButton: !0,
                                        buttonsStyling: !1,
                                        confirmButtonText: "Oui",
                                        cancelButtonText: "Non",
                                        customClass: {
                                            confirmButton: "btn btn-primary",
                                            cancelButton: "btn btn-active-light"
                                        },
                                    }).then(function (t) {
                                        t.value
                                            ? (_modal_update_form.reset(), _modal_update_object.hide())
                                            : "cancel" === t.dismiss
                                    });
                            })
                        });
                    const i = _modal_update_container.querySelector('[data-kt-roles-modal-action="submit"]');
                    i.addEventListener("click", function (_t) {
                        _t.preventDefault(),
                        o &&
                        o.validate().then(function (_t) {
                            console.log("validated!"),
                                "Valid" == _t
                                    ? (i.setAttribute("data-kt-indicator", "on"),
                                        (i.disabled = !0),
                                            setTimeout(()=>{
                                                submit_update_form(LET, CONST, _modal_update_container)
                                            },2e3)
                                    )
                                    : null;
                        });
                    });
                })();
        },
    };
})();

//Variable Global association_droit_role
var droit_role = [];

let getRoleData = (LET, CONST, modalElt) => {
    console.log("2")
    $.ajax({
        url: "/role/listeDroitByAssociation/" + LET.roleId, //à dynamiser
        type: 'get',
        statusCode: {
            200: function (response) {

                //Titre formulaire dynamique
                modalElt.querySelector('.modal-title').textContent = ((LET.roleId !== CONST.roleId_ifAdd)) ? CONST.title_update_modal : CONST.title_add_modal;

                //Remettre valeur libelle sur le champ libelle
                modalElt.querySelector('input[name="role_name"]').value = (LET.roleId !== CONST.roleId_ifAdd) ? response.Role.libelle : "";

                droit_role = response.Droits;

                var html_select_all = `<tr>
                                            <td class="text-gray-800">Accès total
                                                <i class="fas fa-exclamation-circle ms-1 fs-7"
                                                   data-bs-toggle="tooltip"
                                                   title="Permet un accès complet au système"></i>
                                            </td>
                                            <td>
                                                <label class="form-check form-check-sm form-check-custom form-check-solid me-9">
                                                    <input class="form-check-input" type="checkbox"
                                                           value="" id="kt_roles_select_all"/>
                                                    <span class="form-check-label"
                                                          for="kt_roles_select_all">Tout sélectionner</span>
                                                </label>
                                            </td>
                                        </tr>`;

                var html = html_select_all;
                for (const d_r of droit_role) {
                    html += `<tr>
                                                                    <!--begin::Label-->
                                                                    <td class="text-gray-800">${d_r.nom}</td>
                                                                    <!--end::Label-->
                                                                    <!--begin::Input group-->
                                                                    <td>
                                                                        <!--begin::Wrapper-->
                                                                        <div class="d-flex">
                                                                            <!--begin::Checkbox-->
                                                                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                                                                <input class="check_droit form-check-input"
                                                                                       type="checkbox" value="${d_r.id}"
                                                                                       ${d_r.associe ? "checked" : ""}
                                                                                       />
                                                                                <span class="form-check-label">associé</span>
                                                                            </label>
                                                                            <!--end::Checkbox-->
                                                                        </div>
                                                                        <!--end::Wrapper-->
                                                                    </td>
                                                                    <!--end::Input group-->
                                                                </tr>`;
                }

                modalElt.querySelector('#droit_role_association_liste').innerHTML = html;
                onCheckSelectAll();

            },
            500: function (response) {
            }
        }
    })
}

var onCheckSelectAll = () => {

    const e = document.getElementById("kt_modal_update_role_form"),
        t = e.querySelector("#kt_roles_select_all"),
        n = e.querySelectorAll('.check_droit');
    t.addEventListener("change", (t) => {
        n.forEach((e) => {
            e.checked = t.target.checked;
        });
    });
}

var submit_update_form = (LET, CONST, modalElt) => {
    return;
    try {
       droit_role = [];
        //check if all checkboxes are unchecked
        if (!all_checkboxes_are_checked()) throw new Error("Veuillez associer au moins un droit à ce rôle")

        //console.log({droit_role});

        const role_name = modalElt.querySelector('input[name="role_name"]').value

        //Submit form
        $.ajax({
            url: "/role/update/" + LET.roleId, //à dynamiser
            type: 'post',
            dataType: "jsonp",
            data: {nom: role_name,droits: JSON.stringify(droit_role)},
            statusCode: {
                200: function (response) {
                    response = JSON.parse(response.responseText);
                    switch (response.status) {
                        case 200 :
                            Swal.fire({
                                    text: response.msg,
                                    icon: "success",
                                    buttonsStyling: !1,
                                    confirmButtonText: "OK",
                                    customClass: {confirmButton: "btn btn-primary"}
                                }
                            ).then(
                                function (t) {
                                    //t.isConfirmed && n.hide()
                                    const redirect_to = (LET.mode) === 'liste' ? "/role" : "/role/"+ LET.roleId
                                    window.location.replace(redirect_to);
                                })
                            break;
                        case 500 :
                            Swal.fire({
                                text: response.msg,
                                icon: "error",
                                buttonsStyling: !1,
                                confirmButtonText: "OK",
                                customClass: { confirmButton: "btn btn-primary" },
                            })
                    }
                },
                500: function (err) {
                    /*Swal.fire({
                        text: err.responseText,
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "OK",
                        customClass: { confirmButton: "btn btn-primary" },
                    })*/
                }
            }
        })

    } catch (e) {
        Swal.fire({
            text: e.message,
            icon: "error",
            buttonsStyling: !1,
            confirmButtonText: "OK",
            customClass: {confirmButton: "btn btn-primary"},
        })
    } finally {
        const i = modalElt.querySelector('[data-kt-roles-modal-action="submit"]')
        i.removeAttribute("data-kt-indicator"),
            (i.disabled = !1)
    }


}

var all_checkboxes_are_checked = () => {
    const e = document.getElementById("kt_modal_update_role_form"),
        textinputs = e.querySelectorAll('.check_droit');

    var empty = [].filter.call(textinputs, function (el) {
        droit_role.push(
            {
                id: el.value, associe: el.checked
            }
        )
        return !el.checked
    });

    if (textinputs.length == empty.length) {
        return false;
    }
    return true;
}

var changerEtat= (_roleId,_action)=>{
    Swal.fire({
        text: "Confirmation de la "+_action+"?",
        icon: "warning",
        showCancelButton: !0,
        buttonsStyling: !1,
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        customClass: {confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light"},
    }).then(function (t) {
        t.value
            ? $.ajax({
                url: "/role/changerEtat/"+_roleId, //à dynamiser
                type: 'get',
                statusCode: {
                    200: function (response) {
                        if (response.status === 200){
                            Swal.fire({ text: response.msg, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn fw-bold btn-primary" } }).then(function () {
                                //t.row($(n)).remove().draw();
                                window.location.replace("/role/"+_roleId);
                            })
                        }else{
                            Swal.fire({ text: response.msg, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn fw-bold btn-primary" } });
                        }
                    },
                    500: function (response) {
                    }
                }
            })
            : true
    });
}
KTUtil.onDOMContentLoaded(function () {
    KTUsersUpdatePermissions.init();
});
