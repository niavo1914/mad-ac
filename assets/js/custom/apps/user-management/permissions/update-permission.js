"use strict";
var KTUsersUpdatePermission = (function () {
    const t = document.getElementById("kt_modal_update_permission"),
        e = t.querySelector("#kt_modal_update_permission_form"),
        n = new bootstrap.Modal(t);

    const updateData = (button_submit) => {
        $.ajax({
            url: "/droit/update/" + document.getElementById("droit_id2").value, //à dynamiser
            type: 'post',
            dataType: "jsonp",
            data: {nom: document.getElementById("droit_name2").value},
            statusCode: {
                200: function (response) {
                    button_submit.removeAttribute("data-kt-indicator");
                    button_submit.disabled = !1;
                    response = JSON.parse(response.responseText)
                    switch (response.status) {
                        case 200:
                            Swal.fire({
                                    text: response.msg,
                                    icon: "success",
                                    buttonsStyling: !1,
                                    confirmButtonText: "OK",
                                    customClass: {confirmButton: "btn btn-primary"}
                                }
                            ).then(
                                function (t) {
                                    //t.isConfirmed && n.hide()
                                    window.location.replace("/droit");
                                })
                            break
                        case 500:
                            Swal.fire({
                                text: response.msg,
                                icon: "error",
                                buttonsStyling: !1,
                                confirmButtonText: "OK",
                                customClass: {confirmButton: "btn btn-primary"},
                            })
                            break
                    }
                },
                500: function (err) {
                    Swal.fire({
                        text: err.responseText,
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "OK",
                        customClass: {confirmButton: "btn btn-primary"},
                    })
                }
            }
        })
    }

    return {
        init: function () {
            (() => {
                var o = FormValidation.formValidation(e, {
                    fields: {
                        droit_name: {validators: {notEmpty: {message: "Champ obligatoire"}}}
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: ".fv-row",
                            eleInvalidClass: "",
                            eleValidClass: ""
                        })
                    },
                });
                t.querySelector('[data-kt-permissions-modal-action="close"]').addEventListener("click", (t) => {
                    t.preventDefault(),
                        Swal.fire({
                            text: "Êtes-vous sûr de vouloir fermer ?",
                            icon: "warning",
                            showCancelButton: !0,
                            buttonsStyling: !1,
                            confirmButtonText: "Oui",
                            cancelButtonText: "Non",
                            customClass: {confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light"},
                        }).then(function (t) {
                            t.value && n.hide();
                        });
                }),
                    t.querySelector('[data-kt-permissions-modal-action="cancel"]').addEventListener("click", (t) => {
                        t.preventDefault(),
                            Swal.fire({
                                text: "Êtes-vous sûr de vouloir annuler ?",
                                icon: "warning",
                                showCancelButton: !0,
                                buttonsStyling: !1,
                                confirmButtonText: "Oui",
                                cancelButtonText: "Non",
                                customClass: {confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light"},
                            }).then(function (t) {
                                t.value
                                    ? (e.reset(), n.hide())
                                    : "cancel" === t.dismiss
                            });
                    });

                const i = t.querySelector('[data-kt-permissions-modal-action="submit"]');
                i.addEventListener("click", function (t) {
                    t.preventDefault(),
                    o &&
                    o.validate().then(function (t) {
                        console.log("validated!"),
                            "Valid" == t
                                ? (i.setAttribute("data-kt-indicator", "on"),
                                        (i.disabled = !0),
                                        updateData(i)
                                )
                                : null;
                    });
                });
            })();
        },
    };
})();

KTUtil.onDOMContentLoaded(function () {
    KTUsersUpdatePermission.init();
});
