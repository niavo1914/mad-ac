"use strict";
var KTUsersPermissionsList = (function () {
    var t, e;
    const deleteData = (elt,t,n) => {
        $.ajax({
            url: "/droit/changeEtat/"+elt.id, //à dynamiser
            type: 'post',
            dataType : "jsonp",
            data : {etat:0},
            statusCode: {
                200: function (response) {
                    Swal.fire({ text: "Vous avez supprimé " + elt.name + " !.", icon: "success", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn fw-bold btn-primary" } }).then(function () {
                        //t.row($(n)).remove().draw();
                        window.location.replace("/droit");
                    })
                },
                500: function (response) {
                    Swal.fire({ text: elt.name + " n'a pas été supprimé.", icon: "error", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn fw-bold btn-primary" } });
                }
            }
        })
    }

    const reactivateData = (elt,t,n) => {
        $.ajax({
            url: "/droit/changeEtat/"+elt.id, //à dynamiser
            type: 'post',
            dataType : "jsonp",
            data : {etat:1},
            statusCode: {
                200: function (response) {
                    Swal.fire({ text: "Vous avez réactivé " + elt.name + " !.", icon: "success", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn fw-bold btn-primary" } }).then(function () {
                        //t.row($(n)).remove().draw();
                        window.location.replace("/droit");
                    })
                },
                500: function (response) {
                    Swal.fire({ text: elt.name + " n'a pas été réactivé.", icon: "error", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn fw-bold btn-primary" } });
                }
            }
        })
    }

    return {
        init: function () {
            (e = document.querySelector("#kt_permissions_table")) &&
            (e.querySelectorAll("tbody tr").forEach((t) => {
                const e = t.querySelectorAll("td")
                   /* n = moment(e[2].innerHTML, "DD MMM YYYY, LT").format();
                e[2].setAttribute("data-order", n);*/
            }),
                (t = $(e).DataTable({
                    info: !1,
                    order: [[ 1, 'asc' ]],
                    columnDefs: [
                        { orderable: !1, targets: 4 },
                    ],
                    "language": {
                        "sProcessing": "Traitement en cours ...",
                        "sLengthMenu": "Afficher _MENU_ lignes",
                        "sZeroRecords": "Aucun résultat trouvé",
                        "sEmptyTable": "Aucune donnée disponible",
                        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
                        "sInfoEmpty": "Aucune ligne affichée",
                        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
                        "sInfoPostFix": "",
                        "sSearch": "Chercher:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Chargement...",
                        /*"oPaginate": {
                            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
                        },*/
                        "oAria": {
                            "sSortAscending": ": Trier par ordre croissant",
                            "sSortDescending": ": Trier par ordre décroissant"
                        }
                    }
                })),
                document.querySelector('[data-kt-permissions-table-filter="search"]').addEventListener("keyup", function (e) {
                    t.search(e.target.value).draw();
                }),
                e.querySelectorAll('[data-kt-permissions-table-filter="delete_row"]').forEach((e) => {
                    e.addEventListener("click", function (e) {
                        e.preventDefault();
                        const n = e.target.closest("tr"),
                        libelle = n.querySelectorAll("td")[1].innerText,
                        _id = n.querySelectorAll("td")[0].innerText;

                        Swal.fire({
                            text: 'Êtes-vous sûr de vouloir supprimer "' + libelle + '" ?',
                            icon: "warning",
                            showCancelButton: !0,
                            buttonsStyling: !1,
                            confirmButtonText: "Supprimer",
                            cancelButtonText: "Annuler",
                            customClass: { confirmButton: "btn fw-bold btn-danger", cancelButton: "btn fw-bold btn-active-light-primary" },
                        }).then(function (e) {
                            e.value
                                ? deleteData({name:libelle, id:_id},t,n)
                                : "cancel" === e.dismiss &&
                                Swal.fire({ text: libelle + " n'a pas été supprimé.", icon: "error", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn fw-bold btn-primary" } });
                        });
                    });
                }),
                    e.querySelectorAll('[data-kt-permissions-table-filter="reactivate_row"]').forEach((e) => {
                        e.addEventListener("click", function (e) {
                            e.preventDefault();
                            const n = e.target.closest("tr"),
                                libelle = n.querySelectorAll("td")[1].innerText,
                                _id = n.querySelectorAll("td")[0].innerText;

                            Swal.fire({
                                text: 'Êtes-vous sûr de vouloir réactiver "' + libelle + '" ?',
                                icon: "warning",
                                showCancelButton: !0,
                                buttonsStyling: !1,
                                confirmButtonText: "Réactiver",
                                cancelButtonText: "Annuler",
                                customClass: { confirmButton: "btn fw-bold btn-danger", cancelButton: "btn fw-bold btn-active-light-primary" },
                            }).then(function (e) {
                                e.value
                                    ? reactivateData({name:libelle, id:_id},t,n)
                                    : "cancel" === e.dismiss &&
                                    Swal.fire({ text: libelle + " n'a pas été réactivé.", icon: "error", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn fw-bold btn-primary" } });
                            });
                        });
                    }),
                    e.querySelectorAll('[data-kt-permissions-table-filter="update_row"]').forEach((e) => {
                        e.addEventListener("click", function (e) {
                            e.preventDefault();
                            const n = e.target.closest("tr"),
                                _id = n.querySelectorAll("td")[0].innerText,
                                libelle = n.querySelectorAll("td")[1].innerText,
                                code = n.querySelectorAll("td")[2].innerText;


                                $('#kt_modal_update_permission_form #droit_id2').val(_id);
                                $('#kt_modal_update_permission_form #droit_code2').val(code);
                                $('#kt_modal_update_permission_form #droit_name2').val(libelle);

                        });
                    })
            );
        },
    };
})();

KTUtil.onDOMContentLoaded(function () {
    KTUsersPermissionsList.init();
});
