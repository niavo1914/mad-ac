"use strict";
var KTUsersUpdateDetails = (function () {
    const t = document.getElementById("kt_modal_update_details"),
        e = t.querySelector("#kt_modal_update_user_form"),
        update_role_div = document.getElementById("kt_user_view_role_tab"),
        n = new bootstrap.Modal(t);
    var l;
    return {
        init: function () {
            (() => {
                (l = FormValidation.formValidation(e, {
                    fields: {
                        "update-name": {validators: {notEmpty: {message: "Nom est requis"}}},
                        "update-phone": {
                            validators: {
                                notEmpty: {message: "Téléphone est requis"},
                                regexp: {
                                    regexp: "[(+261)] [3][2|3|4|9] [0-9]{2} [0-9]{3} [0-9]{2}$",
                                    message: "Numéro de téléphone non accépté"
                                }
                            }
                        }
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: ".fv-row",
                            eleInvalidClass: "",
                            eleValidClass: ""
                        })
                    },
                })),
                    t.querySelector('[data-kt-users-modal-action="close"]').addEventListener("click", (t) => {
                        t.preventDefault(),
                            Swal.fire({
                                text: "Êtes-vous sûr de vouloir annuler?",
                                icon: "warning",
                                showCancelButton: !0,
                                buttonsStyling: !1,
                                confirmButtonText: "Oui",
                                cancelButtonText: "Non",
                                customClass: {confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light"},
                            }).then(function (t) {
                                t.value
                                    ? (e.reset(), n.hide())
                                    : "cancel" === t.dismiss
                            });
                    }),
                    t.querySelector('[data-kt-users-modal-action="cancel"]').addEventListener("click", (t) => {
                        t.preventDefault(),
                            Swal.fire({
                                text: "Êtes-vous sûr de vouloir annuler?",
                                icon: "warning",
                                showCancelButton: !0,
                                buttonsStyling: !1,
                                confirmButtonText: "Oui",
                                cancelButtonText: "Non",
                                customClass: {confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light"},
                            }).then(function (t) {
                                t.value
                                    ? (e.reset(), n.hide())
                                    : "cancel" === t.dismiss
                            });
                    });
                const o = t.querySelector('[data-kt-users-modal-action="submit"]');
                o.addEventListener("click", function (t) {
                    t.preventDefault(),
                    l &&
                    l.validate().then(function (m) {
                        console.log("validated!"),
                            "Valid" == m
                                ? (o.setAttribute("data-kt-indicator", "on"),
                                    (o.disabled = !0),
                                    setTimeout(function () {
                                        submit_update_form(e, o)
                                    }, 2e3))
                                : Swal.fire({
                                    text: "Désolé, il semble que des erreurs ont été détectées, veuillez réessayer.",
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok",
                                    customClass: {confirmButton: "btn btn-primary"},
                                });
                    })

                }),
                    update_role_div && update_role_div.querySelector('button[name="save_modif_role"]').addEventListener("click",function (m) {
                        m.preventDefault(),
                            submit_update_role();
                    });
            })(),
            (() => {

                })();
        },
    };
})();

var submit_update_role = ()=>{
    try{
        let role_user = []
        //check if all checkboxes are unchecked
        if (!all_role_checkboxes_are_checked(role_user)) throw new Error("Veuillez attacher au moins un rôle à cet responsable")

        const e = document.getElementById("kt_user_view_role_tab"),
         _id = e.querySelector('input[name="user_id"]').value
        console.log({role_user})
        //submit form
        $.ajax({
            url: "/utilisateur/update-role/" + _id, //à dynamiser
            type: 'post',
            dataType: "jsonp",
            data: {roles: JSON.stringify(role_user)},
            statusCode: {
                200: function (response) {
                    response = JSON.parse(response.responseText);
                    switch (response.status) {
                        case 200 :
                            Swal.fire({
                                    text: response.msg,
                                    icon: "success",
                                    buttonsStyling: !1,
                                    confirmButtonText: "OK",
                                    customClass: {confirmButton: "btn btn-primary"}
                                }
                            ).then(
                                function (t) {
                                    //t.isConfirmed && n.hide()
                                    window.location.replace("/utilisateur/"+_id);
                                })
                            break;
                        case 500 :
                            Swal.fire({
                                text: response.msg,
                                icon: "error",
                                buttonsStyling: !1,
                                confirmButtonText: "OK",
                                customClass: { confirmButton: "btn btn-primary" },
                            })
                    }
                },
                500: function (err) {
                    /*Swal.fire({
                        text: err.responseText,
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "OK",
                        customClass: { confirmButton: "btn btn-primary" },
                    })*/
                }
            }
        })

    }catch (e) {
        Swal.fire({
            text: e.message,
            icon: "error",
            buttonsStyling: !1,
            confirmButtonText: "OK",
            customClass: {confirmButton: "btn btn-primary"},
        })
    }finally {

    }
}

var all_role_checkboxes_are_checked = (role_user) => {
    const e = document.getElementById("kt_user_view_role_tab"),
        textinputs = e.querySelectorAll('.check_droit');

    var empty = [].filter.call(textinputs, function (el) {
        role_user.push(
            {
                id: el.value, associe: el.checked
            }
        )
        return !el.checked
    });

    if (textinputs.length == empty.length) {
        return false;
    }
    return true;
}

var _envoi_email_validation = (_userEmail,_idUser)=>{
    Swal.fire({
        title: 'Veuillez Patienter!',
        text: 'Envoi email de validation en cours',
        icon: 'info',
        timerProgressBar: true,
        didOpen: () => {
            Swal.showLoading()
            console.log("didopen")
            $.ajax({
                url:"/utilisateur/sendMailValidation",
                type:'post',
                dataType:'jsonp',
                data:{email:_userEmail},
                statusCode:{
                    200:(response)=>{
                        response = JSON.parse(response.responseText);
                        Swal.close()

                        switch (response.status) {
                            case 200:
                                Swal.fire({
                                    text: "Email envoié",
                                    icon: "success",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok",
                                    customClass: {confirmButton: "btn btn-primary"}
                                })
                                break;
                            case 500:
                                Swal.fire({
                                    text: response.msg,
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok",
                                    customClass: {confirmButton: "btn btn-primary"}
                                })
                                break;
                        }
                    }
                }
            })
        },
        willClose: () => {
            console.log("willclose")
        }
    }).then((result) => {
        /* Read more about handling dismissals below */

    })
}

var changerEtat= (_libelle, _etat, _idUser) =>{

    const verb = _etat === 3 ? 'réactiver' : 'supprimer';
    const Verb = _etat === 3 ? 'Réactiver' : 'Supprimer';
    const cong = _etat === 3 ? 'réactivé' : 'supprimé';

    Swal.fire({
        text: "Etes-vous sûr que vous voulez "+verb+" " + _libelle + " ?",
        icon: "warning",
        showCancelButton: !0,
        buttonsStyling: !1,
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        customClass: { confirmButton: "btn fw-bold btn-danger", cancelButton: "btn fw-bold btn-active-light-primary" },
    }).then(function (e) {
        e.value
            ? $.ajax({
                url: "/utilisateur/changeEtat/"+_idUser, //à dynamiser
                type: 'post',
                dataType : "jsonp",
                data : {etat: _etat},
                statusCode: {
                    200: function (response) {
                        Swal.fire({ text: "Vous avez "+cong+" " + _libelle + " !.", icon: "success", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn fw-bold btn-primary" } }).then(function () {
                            //t.row($(n)).remove().draw();
                            window.location.replace("/utilisateur/"+_idUser);
                        })
                    },
                    500: function (response) {
                        Swal.fire({ text: _libelle + " n'a pas été "+cong+".", icon: "error", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn fw-bold btn-primary" } });
                    }
                }
            })
            : "cancel" === e.dismiss &&
            Swal.fire({ text: libelle + " n'a pas été "+cong+".", icon: "error", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn fw-bold btn-primary" } });
    });
};

var submit_update_form = (modalElt, submitButt) => {
    try {

        const _id = modalElt.querySelector('input[name="update_id"]').value
        const _name = modalElt.querySelector('input[name="update-name"]').value
        const _phone = modalElt.querySelector('input[name="update-phone"]').value

        $.ajax({
            url: "/responsable/update/" + _id, //à dynamiser
            type: 'post',
            dataType: "jsonp",
            data: {nom: _name, phone: _phone},
            statusCode: {
                200: function (response) {
                    response = JSON.parse(response.responseText);
                    switch (response.status) {
                        case 200 :
                            Swal.fire({
                                    text: response.msg,
                                    icon: "success",
                                    buttonsStyling: !1,
                                    confirmButtonText: "OK",
                                    customClass: {confirmButton: "btn btn-primary"}
                                }
                            ).then(
                                function (t) {
                                    //t.isConfirmed && n.hide()
                                    //const redirect_to = (LET.mode) === 'liste' ? "/role" : "/role/"+ LET.roleId
                                    window.location.replace("/responsable/" + _id);
                                })
                            break;
                        case 500 :
                            Swal.fire({
                                text: response.msg,
                                icon: "error",
                                buttonsStyling: !1,
                                confirmButtonText: "OK",
                                customClass: {confirmButton: "btn btn-primary"},
                            })
                    }
                },
                500: function (err) {
                    /*Swal.fire({
                        text: err.responseText,
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "OK",
                        customClass: { confirmButton: "btn btn-primary" },
                    })*/
                }
            }
        })
    } catch (e) {
        console.log(e)
    } finally {
        submitButt.removeAttribute("data-kt-indicator"),
            (submitButt.disabled = !1)
    }
}

KTUtil.onDOMContentLoaded(function () {
    KTUsersUpdateDetails.init();
});
