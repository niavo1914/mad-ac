"use strict";
var KTSigninTwoSteps = (function () {
    var t, n, i;
    return {
        init: function () {
            (t = document.querySelector("#kt_sing_in_two_steps_form")),
                (i = FormValidation.formValidation(t, {
                    fields: {
                        otp: { validators: { notEmpty: { message: "Champ obligatoire" } }}
                    },
                    plugins: { trigger: new FormValidation.plugins.Trigger(), bootstrap: new FormValidation.plugins.Bootstrap5({ rowSelector: ".fv-row" }) },
                })),
                (n = document.querySelector("#kt_sing_in_two_steps_submit")).addEventListener("click", function (e) {
                    e.preventDefault(),
                    i.validate().then(function (i) {
                        "Valid" == i
                            ? (n.setAttribute("data-kt-indicator", "on"),
                                (n.disabled = !0),
                                setTimeout(function () {
                                    /*n.removeAttribute("data-kt-indicator"),
                                        (n.disabled = !1),*/
                                        //submit form
                                        t.submit()
                                }, 1e3))
                            : true
                    });
                });
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTSigninTwoSteps.init();
});
