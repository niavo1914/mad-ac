"use strict";
var KTSigninGeneral = (function () {
    var t, e, i,s;
    return {
        init: function () {
            (s = document.querySelector("#kt_sign_in_submit_google")),
            (t = document.querySelector("#kt_sign_in_form")),
                (e = document.querySelector("#kt_sign_in_submit")),
                (i = FormValidation.formValidation(t, {
                    fields: {
                        identifiant: { validators: { notEmpty: { message: "Mandatory field" } } },
                        password: { validators: { notEmpty: { message: "Mandatory field" } } },
                    },
                    plugins: { trigger: new FormValidation.plugins.Trigger(), bootstrap: new FormValidation.plugins.Bootstrap5({ rowSelector: ".fv-row" }) },
                })),

               /* s.addEventListener("click",function (n){
                    //authentification google
                    n.preventDefault();
                    alert("Hello to you")
                }),*/
                e.addEventListener("click", function (n) {
                    n.preventDefault(),
                        i.validate().then(function (i) {
                            "Valid" == i
                                ? (e.setAttribute("data-kt-indicator", "on"),
                                    (e.disabled = !0),
                                    setTimeout(function () {
                                        /*e.removeAttribute("data-kt-indicator"),
                                            (e.disabled = !1),*/
                                            t.submit()
                                    }, 2e3))
                                : true
                        });
                });
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTSigninGeneral.init();
});
