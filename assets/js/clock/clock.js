"use strict";

// Fonction pour calculer la différence de temps
function calculateTimeDifference(timeString) {
    var parts = timeString.split(':');
    var hours = Number(parts[0]); // Si les heures ne sont pas définies, elles seront considérées comme 0
    var minutes = Number(parts[1]); // Si les minutes ne sont pas définies, elles seront considérées comme 0
    var seconds = Number(parts[2]); // Si les secondes ne sont pas définies, elles seront considérées comme 0

    return (hours * 3600) + (minutes * 60) + seconds;
}

// Fonction pour formater le temps
function formatTime(seconds) {
    const hours = Math.floor(seconds / 3600);
    const minutes = Math.floor((seconds % 3600) / 60);
    const secondss = seconds % 60;

    // Construct the time string
    return String(hours).padStart(2, '0') + ':' + String(minutes).padStart(2, '0') + ':' + String(secondss).padStart(2, '0');
}

var table;
var KTCustomersList = (function () {
    let t,
        e,
        o,
        n,
        event_update_row = () => {
            $('#kt_body').on('click', '[data-kt-customer-table-filter="update_row"]', function (e) {
                e.preventDefault();
                const n = e.target.closest("tr"),
                    _id = n.querySelectorAll("td")[0].innerText,
                    nom = n.querySelectorAll("td")[1].innerText,
                    prenom = n.querySelectorAll("td")[2].innerText,
                    email = n.querySelectorAll("td")[3].innerText,
                    tel = n.querySelectorAll("td")[4].innerText,
                    role = n.querySelectorAll("td")[6].innerText;
                $('#update_id').val(_id)
                $('#update-name').val(nom)
                $('#update-prenom').val(prenom)
                $('#update-email').val(email)
                $('#update-phone').val(tel);
                $('#update-role').val(role);

            })
        }
    return {
        init: function () {
            (n = document.querySelector("#kt_clock_table")) &&
            (n.querySelectorAll("tbody tr").forEach((t) => {
                    const e = t.querySelectorAll("td")
                    /*o = moment(e[5].innerHTML, "DD MMM YYYY, LT").format();
                e[5].setAttribute("data-order", o);*/
                }),
                    (t = $(n).DataTable(configDT(
                        [[1, 'asc']],
                        [
                            {orderable: false, targets: 6},
                            {
                                "targets": [0], // Indexes of columns to hide (0-based)
                                "visible": false,
                            }
                        ],
                        [
                            {
                                extend: 'excelHtml5',
                                filename : 'madacitclock_'+moment().format('YYMMDDHHmmss'),
                                customize: function (xlsx) {
                                    var sheet = xlsx.xl.worksheets['sheet1.xml'];


                                    $('row', sheet).each(function (x) {
                                        if ($('c[r=D' + x + '] t', sheet).text() == 'Total') {
                                            $('row:nth-child(' + x + ') c', sheet).attr('s', '8');
                                        }
                                    });
                                    var lastRowIndex = $('row', sheet).length;
                                    $('c[r="D' + lastRowIndex + '"]', sheet).attr('s', '10');
                                    $('c[r="E' + lastRowIndex + '"]', sheet).attr('s', '10');
                                },
                                customizeData: function (excelData) {
                                    excelData.body.sort(function (a, b) {
                                        return a[2] - b[2];
                                    });

                                    // Ajouter le total sous chaque utilisateur
                                    var currentUserId = '';
                                    var currentUserTotal = 0;
                                    var grandTotal = 0;
                                    excelData.header.splice(0, 1);
                                    excelData.header.splice(5, 1);
                                    for (var i = 0; i < excelData.body.length; i++) {
                                        excelData.body[i].splice(0, 1);
                                        excelData.body[i].splice(5, 1);
                                        var row = excelData.body[i];
                                        if (i == 0) currentUserId = row[1];
                                        if (currentUserId != row[1]) {
                                            excelData.body.splice(i, 0, ['', '', currentUserId, 'Total', formatTime(currentUserTotal)]);
                                            currentUserTotal = 0;
                                            currentUserId = row[1];
                                            i++;
                                        }
                                        if(row[4] == '' || row[4] == null) row[4] = '00:00:00';
                                        currentUserTotal += calculateTimeDifference(row[4]);
                                        grandTotal += calculateTimeDifference(row[4]);
                                    }
                                    excelData.body.push(['', '', currentUserId, 'Total', formatTime(currentUserTotal)]);
                                    excelData.body.push(['', '', '', 'GRAND TOTAL', formatTime(grandTotal)]);
                                },
                            }
                        ]
                    ))),
                    (table = t),
                    document.querySelector('[data-kt-clock-table-filter="search"]').addEventListener("keyup", function (e) {
                        t.search(e.target.value).draw();
                    })
            ),
                event_update_row()
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTCustomersList.init();
    var currentDate = moment();
    var currentDate2 = moment();
    var startOfDay = currentDate.startOf('day');
    var endOfDay = currentDate2.endOf('day');
    changeDate(startOfDay, endOfDay);
});




