const Utilisateur = require('../models').utilisateur;

const {sequelize} = require('../services/sequelize');
const bcrypt = require('bcrypt');
const Utility = require('../services/utilities');
const moment = require("moment");

module.exports = {

    async loginWithoutOTP(req, res) {
        if (req.method === 'GET') return res.render('Authentification/login', {
            error: '',
            success: ''
        });
        if (req.method === 'POST') {
            try {
                if (req.body.identifiant == '' || req.body.password == '') throw "Mandatory field.";
                let user = await Utility.authentification(req.body.identifiant, req.body.password);

                if (!user) throw "User not found.";

                delete user.motDePasse

                let sql = (`SELECT DISTINCT "droitCode"
                            FROM "vue_utilisateur_role_droit"
                            WHERE "userId" = ${user.id}`)

                return Utilisateur.sequelize
                    .query(sql)
                    .then(async accessright => {
                        user = user.dataValues;
                        user.Droits = accessright[0].map(access => access.droitCode);
                        req.session.userData = user;
                        user.from = await moment().add(3, 'hours').format("YYYY-MM-DD HH:mm:ss");
                        console.log(user)
                        const cookie = await Utility.crypt(user, process.env._COOKIE_KEY);
                        res.cookie(process.env._COOKIE_NAME, cookie);
                        return res.redirect('/');
                    })
                    .catch(e => {
                        console.log(e);
                        req.logout();
                        req.flash(' error', "Error!");
                        res.redirect('/login');
                    });
            }
            catch(e) {
                console.log(e);
                return res.render('Authentification/login', {
                    error: e,
                    success: ''
                });
            }
        } else {
            res.render('Authentification/login', {error: req.flash('error'), success: req.flash('success')});
        }
    },

    async changePassword(req, res) {
        const urlMdp = req.params.urlMdp;
        const saltRounds = 10;

        let user_email= await Utilisateur.findOne({
            where:{
                urlMotDePasse : urlMdp
            }
        })

        req.session.user_email=user_email.email

        return Utilisateur.findOne({
            where: {
                urlMotDePasse: urlMdp
            },
        }).then((user) => {
            if (!user) {
                return res.redirect('/');
            }
            if (req.method === 'POST') {
                let error
                let password = req.body.password
                let confirmPassword = req.body.confirmPassword
                let regex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g")


                if(password==="" || confirmPassword===""){
                    error = "Champ obligatoire"
                    req.flash('error', error)
                    return res.redirect('back')
                }

                if(password.length<8 || password.length>20){
                    error = "Veuillez saisir un mot de passe valide"
                    req.flash('error', error)
                    return res.redirect('back')
                }

                if (password !== confirmPassword) {
                    error = "Les valeurs sont différentes"
                    req.flash('error', error)
                    return res.redirect('back')
                }

                if(regex.test(password) === false){
                    error = "Veuillez saisir un mot de passe valide"
                    req.flash('error', error)
                    return res.redirect('back')
                }


                bcrypt.genSalt(saltRounds, function(err, salt) {
                    bcrypt.hash(password, salt, async function(err, hash) {
                        console.log("update hash")
                        let t1;
                        try{
                            t1 = await sequelize().transaction();
                            await user.update({
                                motDePasse: hash,
                                etat: 1,
                                urlMotDePasse: null,
                                salt: salt
                            },{transaction:t1})

                            await t1.commit();
                            req.flash('success', 'Mot de passe modifié avec succès!');
                            return res.redirect('/login');
                        }catch (e) {
                            console.log(e)
                            await t1.rollback();
                            req.flash('error', e.message);
                            return res.redirect('back');
                        }
                    });
                });
            }else{
                return res.render('Authentification/changePassword', {user: user, error: req.flash('error')});
            }
        }).catch((err) => console.log(err.message));
    }


}