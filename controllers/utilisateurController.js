const Utilisateur = require('../models').utilisateur;
const Role = require('../models').role;
const Utilisateur_Role = require('../models').utilisateur_role;
const {sequelize} = require('../services/sequelize');
const {Op} = require('sequelize');
const bcrypt = require('bcrypt');
const {SUPERADMIN, Etat, ERREUR_TECH_MESSAGE, ERREUR_PROBLEME_DROIT} = require('../config/const')
const {ACTIF} = require('../config/const')
const {_checkDroits} = require('../services/utilities')
const Ajv = require('ajv'), AjvService = require('../services/ajvService'),
    AjvSchema = require('../services/ajv/ajvValidateSocleUser')


module.exports = {
    async listerUtilisateur(req, res) {
        let users
        let roles
        users = await Utilisateur.findAll();
        for (var i = 0; i < users.length; i++) {
            const sql = `SELECT role_id
                         from utilisateur_role
                         where utilisateur_id = ${users[i].id}`
            let result = await Role.sequelize.query(sql).then(data => {
                return data[0];
            })
            users[i].RoleAssocie = result[0].role_id;
        }

        roles = await Role.findAll();

        //console.log({users});
        return res.render('Utilisateur/liste', {
            users,
            roles,
            titre: "Utilisateur",
            soustitre: "Liste",
            groupe: "Habilitation",
            error: '',
        })
    },

    async creerUtilisateur(req, res) {
        let ret = {
            CodeRetour: 500,
            DescRetour: "",
            DetailRetour: ERREUR_TECH_MESSAGE
        }

        let t1;


        //CHECK JSON FORMAT INPUT API
        const AJV = new Ajv({allErrors: true, allowUnionTypes: true});
        const ajvSchema = AjvSchema.getSchema_createUtilisateur();
        const ajvValidator = AJV.compile(ajvSchema)


        let _nom = req.body.nom.trim()
        const _prenom = req.body.firstName.trim()
        const _email = (req.body.email) ? req.body.email.toLowerCase().trim() : null
        const _phone = (req.body.phone) ? req.body.phone.trim() : null

        try {

            //check donné invalide
            if (!_nom) throw new Error("Champ vide")
            if (_nom.length > 100) throw new Error("Longueur invalide")
            if (_prenom.length > 100) throw new Error("Longueur invalide")

            if (!ajvValidator(req.body)) {
                ret = AjvService.validate(ajvValidator.errors)
                return
            }

            // mettre le nom en CAPITALE
            if (_nom) {
                let listNom = _nom.split(' ');
                let nom = "";
                listNom.forEach(function (value, i) {
                    if (i == 0) nom = value.toUpperCase();
                    else nom += " " + value
                });
                _nom = nom;
            }

            const uniq = await Utilisateur.count({where: {prenom: _prenom}});
            if (uniq > 0) {
                ret.CodeRetour = 300
                throw new Error("Identifiant déjà existant");
            }

            t1 = await sequelize().transaction();

            //Mot de passe généré automatiquement et crypté
            const salt = await bcrypt.genSalt(1)
            const hash = await bcrypt.hash(process.env._MDP_PARDEFAUT, salt)


            const newUser = await Utilisateur.create({
                email: _email,
                nom: _nom,
                prenom: _prenom,
                tel: _phone,
                motDePasse: hash,
                salt: salt,
                etat: Etat.ACTIF
            }, {transaction: t1});

            Utilisateur_Role.removeAttribute("id");
            await Utilisateur_Role.create({
                utilisateur_id : newUser.dataValues.id,
                role_id : 5,
                createdAt : Date.now(),
                updatedAt : Date.now()

            }, {transaction: t1});
            ret.CodeRetour = 200;
            ret.DescRetour = 'succès';
            ret.DetailRetour = 'Mot de passe : ' + process.env._MDP_PARDEFAUT;
            //Après envoi email, si OK
            await t1.commit();
        } catch (e) {
            console.log(e)
            if (ret.CodeRetour != 500) ret.DetailRetour = e.message
            else ret.DescRetour = e.message

            await t1.rollback()
        } finally {
            return res.json(ret);
        }
    },

    async editerUtilisateur(req, res) {
        let ret = {
            CodeRetour: 500,
            DescRetour: "",
            DetailRetour: ERREUR_TECH_MESSAGE
        }, t1;

        const _id = req.params.id
        let _nom = (req.body.nom) ? req.body.nom.trim() : null
        let _prenom = (req.body.prenom) ? req.body.prenom.trim() : null
        const _phone = (req.body.phone) ? req.body.phone.trim() : null
        const _mail = (req.body.mail) ? req.body.mail.trim() : null
        const _role = (req.body.role) ? req.body.role : null

        try {
            //CHECK JSON FORMAT INPUT API
            const AJV = new Ajv({allErrors: true, allowUnionTypes: true});
            const ajvSchema = AjvSchema.getSchema_updateUtilisateur();
            const ajvValidator = AJV.compile(ajvSchema)
            if (!ajvValidator(req.body)) {
                ret = AjvService.validate(ajvValidator.errors)
                return;
            }

            //check donné invalide
            if (!_nom || _nom.length > 100) throw new Error("Nom invalide")
            if (_prenom && _prenom.length > 100) throw new Error("Prénom invalide")

            t1 = await sequelize().transaction();
            //   const old_user = await Utilisateur.findByPk(_id);
            const old_user = await Utilisateur.findOne({
                where: {
                    id: _id
                }
            })
            if (old_user.prenom != _prenom) {
                let uniq = await Utilisateur.count({where: {prenom: _prenom}});
                if (uniq > 0) {
                    ret.CodeRetour = 300
                    throw new Error("Identifiant déjà existant");
                }
            }

            if (old_user.etat == Etat.INACTIF) {
                ret.CodeRetour = 300
                throw new Error("Cet Utilisateur est désactivé")
            }

            const sql = `SELECT role_id
                         from utilisateur_role
                         where utilisateur_id = ${old_user.id}`
            let result = await Role.sequelize.query(sql).then(data => {
                return data[0];
            });

            if (old_user.tel != _phone || old_user.nom.toLowerCase().trim() != _nom.toLowerCase() || old_user.prenom != _prenom || old_user.email != _mail || result[0].role_id != _role) {
                let updated = {
                    email: _mail
                };
                let comment = []
                if (old_user.tel != _phone) {
                    updated = {tel: _phone}
                    comment.push({
                        Champ: "Tel",
                        Ancienne_Valeur: old_user.tel,
                        Nouvelle_Valeur: _phone
                    });
                }
                if (old_user.nom.toLowerCase().trim() != _nom.toLowerCase()) {
                    // mettre le nom en CAPITALE
                    let listNom = await _nom.split(' ');
                    let nom = "";
                    listNom.forEach(function (value, i) {
                        if (i == 0) nom = value.toUpperCase();
                        else nom += " " + value
                    });
                    _nom = nom;
                    updated = {...updated, nom: _nom}
                    comment.push({
                        Champ: "Nom",
                        Ancienne_Valeur: old_user.nom,
                        Nouvelle_Valeur: _nom
                    });
                }

                if (old_user.prenom != _prenom) {
                    updated = {prenom: _prenom}
                    comment.push({
                        Champ: "Prenom",
                        Ancienne_Valeur: old_user.prenom,
                        Nouvelle_Valeur: _prenom
                    });
                }

                if (result[0].role_id != _role) {
                    await Utilisateur_Role.update({
                        role_id: _role
                    }, {
                        where: {
                            utilisateur_id: old_user.id,
                            role_id: result[0].role_id
                        }
                    })
                }
                await Utilisateur.update(updated, {where: {id: _id}, transaction: t1});
            }

            ret.CodeRetour = 200;
            ret.DescRetour = 'succès';
            ret.DetailRetour = 'Enregistrement réussi';

            await t1.commit();

        } catch (e) {
            console.log(e);
            if (ret.CodeRetour == 500) ret.DescRetour = e.message
            else ret.DetailRetour = e.message

            await t1.rollback();
        } finally {
            return res.json(ret)
        }
    },
    async changerEtat(req, res) {
        let _id = req.params.id;
        let _etat = req.body.etat;

        let ret = {}, t1;
        try {
            const checkEtat = new RegExp('^[0|1]$');
            if (!checkEtat.test(_etat)) throw new Error("Etat non valide")
            t1 = await sequelize().transaction();

            await Utilisateur.update({
                etat: _etat
            }, {where: {id: _id}, transaction: t1})

            ret = {
                status: 200,
                desc: 'succès',
                msg: 'Enregistrement réussi'
            }
            await t1.commit();

        } catch (e) {
            console.log(e)
            ret = {
                status: 500,
                desc: 'echec',
                msg: e.message
            }

            await t1.rollback();
        } finally {
            res.json(ret);
        }
    },
    async resetPassword(req, res) {
        const id = req.params.id;
        const salt = await bcrypt.genSalt(1)
        const hash = await bcrypt.hash(process.env._MDP_PARDEFAUT, salt);
        await Utilisateur.update({
            salt : salt,
            motDePasse : hash
        }, {
            where : {
                id : id
            }
        });
        return res.status(200).send(process.env._MDP_PARDEFAUT);
    },

    async changePassword(req, res) {
        if (req.method == 'POST') {
            try {

                const saltRounds = 10;

                const _current_password = req.body.currentPass;
                const _new_password = req.body.newPass;
                const _confirm_password = req.body.confirmPass;

                if (!_current_password || !_new_password || !_confirm_password) throw 'Fields mandatory.';

                bcrypt.genSalt(saltRounds, function (err, salt) {
                    bcrypt.hash(_new_password, salt, async function (err, hash) {

                        try {
                            // if (_current_password.length < 8 || _new_password.length < 8 || _confirm_password.length < 8 || _current_password.length > 20 || _new_password.length > 20 || _confirm_password.length > 20) throw new Error("Veuillez respecter la longueur autorisée")
                            //if(regex.test(_current_password) === false || regex.test(_new_password) === false) throw new Error("Veuillez saisir un mot de passe valide")

                            const user = res.locals.user;
                            const hash_current_pass = await bcrypt.hashSync(_current_password, user.salt);
                            if (_new_password != _confirm_password) throw "The new passwords are not the same."
                            if (user.motDePasse != hash_current_pass) throw "The password is incorrect."

                            await Utilisateur.update({
                                motDePasse: hash,
                                salt: salt
                            }, {where : {
                                id : user.id
                                }})

                            return res.status(200).send("OK");
                        } catch (e) {
                            console.log(e)
                            return res.status(500).send(e);
                        }

                    })
                })
            } catch (e) {
                console.log(e)
                await transaction.rollback()
                return res.status(500).send(e);
            }
        }
    },

    async exceptionlunch(req, res) {
        console.log("AO");
        console.log(req.params);
        await Utilisateur.update({
            exceptionlunch : req.params.value == '1' ? true : false
        },{
            where : {
                id : req.params.id
            }
        }).then(data=>{
            console.log("VITA")
        });
        return res.status(200).send("ok");
    }

};