const moment = require('moment');
const Clock = require('../models').clock;

module.exports = {
    async clockIn(req, res) {
        try {
            const user = res.locals.user;
            const check = await Clock.findAll({
                where: {
                    clockOut: null,
                    user_id : user.id
                }
            });
            if (check.length > 0) throw 'Action not permitted.'
            const clock = await Clock.create({
                user_id: user.id,
            }).catch(e => {
                console.log(e);
                throw 'erreur interne';
            });
            return res.redirect('/');
        } catch (e) {
            console.log(e);
            return res.redirect('/?message=' + e);
        }
    },

    async clockOut(req, res) {
        try {
            const user_id = req.originalUrl.includes('/clock/out') ? res.locals.user.id : req.params.id;
            const check = await Clock.findAll({
                where: {
                    clockOut: null,
                    user_id: user_id
                }
            });
            if (check.length > 1) throw 'Action not permitted.'
            await Clock.update({
                clockOut: Date.now(),
                updateAt: Date.now(),
                launch : req.params.launch == 1 ? true : false
            }, {
                where: {
                    clockOut: null,
                    user_id: user_id
                }
            });
            if (req.originalUrl.includes('/clock/out')) return res.redirect('/');
            else return res.redirect('/clock');
        } catch (e) {
            console.log(e);
            return res.redirect('/?message=' + e);
        }
    },

    async listerClock(req, res) {
        let error = "";
        if (req.query.message) error = req.query.message;
        return res.render("Clock/liste", {
            titre: 'Historique',
            soustitre: 'Liste',
            error: error,
        })
    },

    async filterClock(req, res) {
        try {
            const startDate = req.body.startDate;
            const finishDate = req.body.finishDate;
            let action1 = "  class=\"btn btn-sm btn-light-primary btn-active-light-primary\"\n" +
                "                                                           data-kt-menu-trigger=\"click\"\n" +
                "                                                           data-kt-menu-placement=\"bottom-end\">";
            let action2 = "<!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->\n" +
                "                                                            <span class=\"svg-icon svg-icon-5 m-0\">\n" +
                "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>\n" +
                "                                                            <!--end::Svg Icon--></a>";
            let action=""
            const sql = `SELECT CASE
                                    WHEN clock."clockOut" IS NULL THEN NULL
                                    ELSE TO_CHAR(clock."clockOut", 'YYYY/MM/DD HH24:MI:SS')
                                    END                                     as "clockOut",
                                TO_CHAR(clock."clockIn", 'YYYY/MM/DD HH24:MI:SS') as "clockIn",
                                nom,
                                clock.id                                    as id,
                                user_id,
                                CASE
                                    WHEN clock."clockOut" IS NULL THEN '[' || clock.id || ',"' || nom || '", "' || prenom ||
                                                               '", "' || TO_CHAR(clock."clockIn", 'DD/MM/YYYY HH24:MI:SS') ||
                                                               '","", ""]'
                                    ELSE '[' || clock.id || ',"' || nom || '", "' || prenom || '", "' ||
                                         TO_CHAR(clock."clockIn", 'DD/MM/YYYY HH24:MI:SS') || '","' ||
                                         TO_CHAR(clock."clockOut", 'DD/MM/YYYY HH24:MI:SS') || '", "' ||
                                         TO_CHAR(EXTRACT(HOUR FROM (clock."clockOut" - clock."clockIn")) + EXTRACT(DAY FROM (clock."clockOut" - clock."clockIn")) * 24, 'FM9999') || ':' ||
                                         TO_CHAR(EXTRACT(MINUTE FROM (clock."clockOut" - clock."clockIn")), 'FM00') || ':' ||
                                         TO_CHAR(EXTRACT(SECOND FROM (clock."clockOut" - clock."clockIn")), 'FM00') ||
                                         '"]'
                                    END                                     AS result
                         FROM "clock"
                                  JOIN
                              "utilisateur" ON clock.user_id = utilisateur.id
                         WHERE clock."clockIn" >= '${startDate}'
                           AND clock."clockIn" <= '${finishDate}'
                         ORDER BY "clockIn" DESC`;
            const data = await Clock.sequelize.query(sql).then(data => {
                return data[0];
            });
            let result = [];
            for (var i = 0; i < data.length; i++) {
                let temp = JSON.parse(data[i].result);
                if (data[i].clockOut) {
                    let tempHour = temp[5].split(':');
                    tempHour[0] = String(tempHour[0]).padStart(2, '0');
                    tempHour[1] = String(tempHour[1]).padStart(2, '0');
                    tempHour[2] = String(tempHour[2]).padStart(2, '0');
                    temp[5] = tempHour.join(':');
                }
                if (!data[i].clockOut) action = "<a onclick=\"clockout(" + data[i].user_id + ",'" + data[i].nom + "')\" " + action1 + "Clock Out" + action2;
                else action = "<a onclick=\"modify(" + data[i].id + ",'" + data[i].nom + "','" + data[i].clockIn + "','" + data[i].clockOut + "')\" " + action1 + "Modifier" + action2;
                temp.push(action);
                result.push(temp);
            }
            return res.status(200).json(result)
        } catch (e) {
            console.log(e);
            return res.redirect('/clock/?message=' + e);
        }
    },

    async updateClock(req, res) {
        try {
            let clockIn = req.body.clockIn;
            let clockOut = req.body.clockOut;
            let id = req.body.id;
            if (!clockIn || !clockOut || !id) throw 'Champs obligatoire.';
            clockIn = moment(clockIn, 'DD/MM/YYYY HH:mm:ss');
            clockOut = moment(clockOut, 'DD/MM/YYYY HH:mm:ss');
            if (clockOut > moment().add(3, 'hours')) throw 'Clock Out incorrect';
            if (clockIn > clockOut) throw 'Clock In doit être inférieur au Clock Out';
            await Clock.update({
                clockIn: clockIn.format('YYYY-MM-DD HH:mm:ss'),
                clockOut: clockOut.format('YYYY-MM-DD HH:mm:ss')
            }, {
                where: {id: id}
            });
            const seconds = clockOut.diff(clockIn, 'seconds');
            const hours = Math.floor(seconds / 3600);
            const minutes = Math.floor((seconds % 3600) / 60);
            const secondss = seconds % 60;

            // Construct the time string
            const temps = String(hours).padStart(2, '0') + ':' + String(minutes).padStart(2, '0') + ':' + String(secondss).padStart(2, '0');
            return res.status(200).send({
                clockIn : clockIn.format('DD/MM/YYYY HH:mm:ss'),
                clockOut: clockOut.format('DD/MM/YYYY HH:mm:ss'),
                tempsPasse: temps
            });
        } catch (e) {
            console.log(e);
            return res.status(500).send(e);
        }
    },

    async launch(req, res) {
        try {
            await Clock.update({
                launch: false
            }, {
                where: {
                    id: req.params.id
                }
            })
            return res.json('ok')
        } catch (e) {
            console.log(e);
            return res.json('erreur')
        }

    }
}