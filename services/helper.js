const moment = require('moment');
const {CONST_VUE} = require("../config/const");
const {Etat} = require('../config/const')
const {formatString, formatPhoneNumber, NowTimestampFormat} = require('./utilities')

module.exports = (app) => {
    app.locals.somevar = "hello world";

    app.locals.someHelper = function (name) {
        return ("hello " + name);
    }

    app.locals.formatString = formatString

    app.locals.limitLength = (text,length_max) =>{
        if (text.length>length_max) text = text.substr(0, length_max)+"..."
        return text;
    }

    app.locals.formatPhoneNumber = formatPhoneNumber

    app.locals.moment = moment;

    app.locals.const_vue = CONST_VUE;

    app.locals.Etat = Etat;

    app.locals.NowTimestampFormat = NowTimestampFormat

    return app;
}