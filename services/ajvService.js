const Ajv = require("ajv");
const addFormats = require("ajv-formats");
module.exports = {
    validate(errors) {
        let details;
        const manquant = errors.filter(function (x) {
            return x.keyword === "required"
        });
        const incomplet = errors.filter(function (x) {
            return x.keyword === "minLength" && x.params.limit === 1
        });
        const format = errors.filter(function (x) {
            return x.keyword === "format";
        });
        const incorrect = errors.filter(function (x) {
            return x.keyword === "pattern" || x.keyword === "type" || x.keyword === "enum" || x.keyword === "maxLength" || x.keyword === "minimum" || x.keyword === "maximum";
        });
        if (manquant.length !== 0) {
            details = [];
            manquant.filter(function (x) {
                details.push(x.params.missingProperty);
            });
            return {
                "CodeRetour": 703,
                "DescRetour": "Champ manquant",
                "DetailRetour": "",
                "Data": [...new Set(details)]
            };
        }
        if (incomplet.length !== 0) {
            details = [];
            incomplet.filter(function (x) {
                const temp = x.instancePath.split('/');
                details.push(temp[temp.length - 1]);
            });
            return {
                "CodeRetour": 701,
                "DescRetour": "Champ vide",
                "DetailRetour": "",
                "Data": [...new Set(details)]
            };
        }
        if (incorrect.length !== 0) {
            details = [];
            incorrect.filter(function (x) {
                console.log({x})
                const temp = x.instancePath.split('/');
                details.push(temp[temp.length - 1]);
            });
            return {
                "CodeRetour": 702,
                "DescRetour": "Champ incorrect",
                "DetailRetour": "",
                "Data": [...new Set(details)]
            };
        }
        if (format.length !== 0) {
            details = [];
            format.filter(function (x) {
                const temp = x.instancePath.split('/');
                details.push(temp[temp.length - 1]);
            });
            return {
                "CodeRetour": 703,
                "DescRetour": "Format incorrect",
                "DetailRetour": "",
                "Data": [...new Set(details)]
            };
        }
    }
}