const env = process.env._NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.js')[env];
const Sequelize = require("sequelize");

module.exports.sequelize = () => {
    let sequelize
    if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
    } else {
        sequelize = new Sequelize(config.database, config.username, config.password, config);
    }

    return sequelize;
}