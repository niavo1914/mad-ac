const regexEmail = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"
const regexNumeroCheck="^\\(\\+261\\)\\s{0,1}3[2348]{1}\\s{0,1}[0-9]{2}\\s[0-9]{3}\\s{0,1}[0-9]{2}$"

module.exports = {
    getSchema_updateUtilisateur(){
        return {
            "type":"object",
            "required":["nom","phone"],
            "properties":{
                "nom":{
                    "type":["string"],
                    "minLength":1,
                    "maxLength": 80
                }
                ,
                "phone":{
                    "oneOf": [
                        {"type": ["string"]},
                    ]
                }
            }
        }
    },



    getSchema_createUtilisateur(){
        return {
            "type":"object",
            "required":["nom","firstName"],
            "properties":{
                "nom":{
                    "type":["string"],
                    "minLength":1,
                    "maxLength": 80
                },
                "firstName":{
                    "type":["string"],
                    "minLength":1,
                    "maxLength": 80
                }
            }
        }
    },

}