const Utilisateur = require('../models').utilisateur

const bcrypt = require("bcrypt");
const {ACTIF, ERREUR_TECH_MESSAGE} = require("../config/const");
const NodeRSA = require('node-rsa')
const jwt = require('jsonwebtoken');

const {Navigator} = require("node-navigator");
const navigator = new Navigator();

async function getSalt(email) {
    const user = await Utilisateur.findOne({
        where: {
            prenom: email,
        },
    })
    return (user) ? user.salt : null;
}

async function verifyAuth(email, password) {

    let salt = await getSalt(email)

    if (salt) {
        let hash = bcrypt.hashSync(password, salt);

        return Utilisateur.findOne({
            attributes: ['id', 'nom', 'email', 'tel', 'etat'],
            where: {
                prenom: email,
                motDePasse: hash,
                etat: ACTIF
            },
        }).then(user => {
            return user;
        });
    }

    return null


}


function decrypt_data(data) {
    let mdp = process.env._KEY_DECRYPTAGE.toString() + "";
    mdp = mdp.split("\\n").join("\n");
    let key_private = new NodeRSA(mdp)
    let resultat = key_private.decrypt(data, "utf8")
    return resultat
}

module.exports = {
    async crypt(data, key) {
        return await jwt.sign(JSON.stringify(data), key);
    },

    async decrypt(data, key) {
        return await jwt.verify(data, key);
    },
    async authentification(email, password) {


        return await verifyAuth(email, password);

    },
}
