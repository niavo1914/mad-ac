const {ERREUR_PROBLEME_DROIT} = require('../config/const');
const Utility = require('../services/utilities');
const moment = require("moment");
const Utilisateur = require('../models').utilisateur;
const Clock = require('../models').clock;

hasAccessRight = function (droit, user) {
    let retour = false;
    let accessArray = user.Droits;
    for (let i = 0; i < droit.length; i++) {
        if (accessArray.includes(droit[i])) {
            retour = true;
            return retour;
        }
    }
    return retour;
};

module.exports.isLoggedIn = async function (req, res, next) {
    if (req.cookies[process.env._COOKIE_NAME]) {
        let user = await Utility.decrypt(req.cookies[process.env._COOKIE_NAME], process.env._COOKIE_KEY);
        let hoursDifference = moment().add(3, 'hours').diff(moment(user.from), 'hours');
        if (hoursDifference > process.env._COOKIE_TIME) return res.redirect('/logout');
        let userNew = await Utilisateur.findOne({
            where: {
                id: user.id,
                etat: '1'
            }
        }).then(data => {
            if (data) return data.dataValues;
            else return null;
        });
        if (!userNew) return res.redirect('/login');
        let sql = (`SELECT DISTINCT "droitCode"
                    FROM "vue_utilisateur_role_droit"
                    WHERE "userId" = ${userNew.id}`)
        userNew.Droits = await Utilisateur.sequelize
            .query(sql)
            .then(async accessright => {
                const droits = accessright[0].map(access => access.droitCode);
                return droits;
            })
            .catch(e => {
                console.log(e);
                req.logout();
                req.flash(' error', "Error!");
                res.redirect('/login');
            });
        req.session.userData = userNew;
        res.locals.user = userNew;
        const toToken = {
            ...userNew,
            from: user.from
        }
        delete toToken.motDePasse;
        delete toToken.urlMotDePasse;
        delete toToken.createdAt;
        delete toToken.updatedAt;
        delete toToken.salt;
        const cookie = await Utility.crypt(toToken, process.env._COOKIE_KEY);
        res.cookie(process.env._COOKIE_NAME, cookie);
        next();
    } else return res.redirect('/logout');
};


module.exports.checkDroit = (droit) => {
    return async function (req, res, next) {
        if (await hasAccessRight(droit, res.locals.user)) {
            next();
        } else {
            return res.redirect('/logout');
        }
    }
};

module.exports.updateClock = async function (req, res, next) {
    let sql = `SELECT to_char(now(), 'YYYY-MM-DD HH24:MI:SS') as "maintenant", clock.*, utilisateur.exceptionlunch
               FROM "clock"
                        JOIN utilisateur ON clock.user_id = utilisateur.id
               WHERE clock."clockOut" IS NULL`;
    const clock = await Clock.sequelize.query(sql).then(data => {
        if (data[0].length > 0) return data[0];
        else return [];
    });
    for (var i = 0; i < clock.length; i++) {
        if (clock[i].exceptionlunch == false) {
            const noww = moment(clock[i].maintenant);
            const clockIn = moment(clock[i].clockIn);
            const diff = await noww.diff(clockIn, 'seconds');
            if (diff >= 18000) {
                const newDate = clockIn.add(7200, 'seconds').format("YYYY-MM-DD HH:mm:ss");
                await Clock.update({
                    clockOut: newDate,
                    updatedAt: Date.now(),
                    launch: true
                }, {
                    where: {
                        id: clock[i].id
                    }
                })
            }
        }
    }
    next();
};