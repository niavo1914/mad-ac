var express = require('express');
var router = express.Router();
const {clockIn, clockOut, listerClock, updateClock, filterClock, launch} = require('../controllers/clockController');
const {checkDroit} = require("../services/middlewares");

module.exports = (io) => {
    router.get('/in', checkDroit(['clock']), clockIn);
    router.get('/out/:launch', checkDroit(['clock']), clockOut);
    router.get('/outother/:id', checkDroit(['rectificationClock']), clockOut);
    router.get('/', checkDroit(['historiqueClock']), listerClock);
    router.post('/', checkDroit(['rectificationClock']), updateClock);
    router.post('/filter', checkDroit(['historiqueClock']), filterClock);
    router.get('/launch/:id', launch);

    return router;
}