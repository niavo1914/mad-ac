var express = require('express');
var router = express.Router();
var {checkDroit} = require('../services/middlewares');
const {resetPassword} = require("../controllers/utilisateurController");


module.exports = (io) => {
    const {listerUtilisateur, creerUtilisateur, editerUtilisateur, changerEtat, changePassword, exceptionlunch} = require('../controllers/utilisateurController');
    router.get('/', checkDroit(['listUser']), listerUtilisateur);
    router.post('/create', checkDroit(['createUser']), creerUtilisateur)
    router.post('/update/:id', checkDroit(['updateUser']), editerUtilisateur)
    router.post('/changeEtat/:id', checkDroit(['updateUser']), changerEtat)
    router.post('/changePassword/', changePassword)
    router.get('/resetPassword/:id',checkDroit(['updateUser']), resetPassword);
    router.get('/exception/:id/:value',checkDroit(['updateUser']), exceptionlunch)

    return router;
};
