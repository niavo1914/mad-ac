const express = require('express');
const router = express.Router();

module.exports = (io) => {

    const {
        loginWithoutOTP,

    } = require('../controllers/authController');

    router.get('/logout', (req, res) => {
        if(req.cookies[process.env._COOKIE_NAME]) res.clearCookie(process.env._COOKIE_NAME);
        return res.redirect('/login');
    });

    router.get('/login', loginWithoutOTP);
    router.post('/login', loginWithoutOTP);


    return router;
}