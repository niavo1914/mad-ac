var express = require('express');
var router = express.Router();
const {isLoggedIn, updateClock} = require('../services/middlewares');
const moment = require("moment");
const Clock = require('../models').clock;
const Utilisateur = require('../models').utilisateur;


module.exports = (io) => {
    const utilisateur = require('./utilisateur')(io);
    const auth = require('./auth')(io);
    const clock = require('./clock')(io);

    /* GET home page. */
    router.get('/', isLoggedIn, updateClock, async function (req, res, next) {
        let sql = `SELECT to_char(now(), 'YYYY-MM-DD HH24:MI:SS') as "maintenant", clock.*, utilisateur.exceptionlunch
                   FROM "clock"
                            JOIN utilisateur ON clock.user_id = utilisateur.id
                   WHERE clock."clockOut" IS NULL
                     AND user_id = ${res.locals.user.id}`;

        const clock = await Clock.sequelize.query(sql).then(data => {
            if (data[0].length > 0) return data[0][0];
            else return null;
        });
        let data = {}
        if (clock) {
            const date1 = moment(clock.maintenant);
            const date2 = moment(clock.clockIn);
            data = {
                date: date2,
                dateFormatted: date2.format('DD/MM/YYYY HH:mm:ss'),
                clockOut: true,
                id: clock.id,
                launch: clock.launch,
                timePassed: date1.diff(date2, 'seconds'),
                exception: clock.exceptionlunch
            }
        } else {
            const clock = await Clock.findOne({
                where: {
                    launch: true,
                    user_id: res.locals.user.id
                }
            }).then(data => {
                if (data) return data.dataValues;
                else return null;
            });
            data = {
                date: null,
                dateFormatted: null,
                clockOut: false,
                id: clock ? clock.id : null,
                launch: clock ? clock.launch : null,
                timePassed: null,
                exception: null
            }
        }
        let error = "";
        if (req.query.message) error = req.query.message;
        return res.render('index', {
            titre: 'Dashboard',
            soustitre: 'Welcome',
            error: error,
            user: res.locals.user,
            clock: data
        });
    });

    /* empty page template */
    router.get('/empty', function (req, res, next) {
        res.render('empty', {titre: 'Empty', soustitre: 'page Vide'});
    });

    router.use('/',updateClock, auth);
    router.use('/clock', isLoggedIn, updateClock, clock);
    router.use('/utilisateur', isLoggedIn, updateClock, utilisateur);

    return router;
};
