create table "SequelizeMeta"
(
    name varchar(255) not null
        primary key
);

alter table "SequelizeMeta"
    owner to postgres;

create table droit
(
    id          serial
        constraint undefined_droit_pkey
            primary key,
    etat        varchar(1)               not null,
    libelle     varchar(100)             not null,
    code        varchar(50)              not null,
    "createdAt" timestamp with time zone not null,
    "updatedAt" timestamp with time zone not null
);

alter table droit
    owner to postgres;

create table role
(
    id          serial
        constraint undefined_role_pkey
            primary key,
    etat        varchar(1)               not null,
    libelle     varchar(100)             not null,
    description text,
    "createdAt" timestamp with time zone not null,
    "updatedAt" timestamp with time zone not null
);

alter table role
    owner to postgres;

create table utilisateur
(
    id              serial
        constraint undefined_utilisateur_pkey
            primary key,
    "motDePasse"    text,
    etat            varchar(1) default '0'::character varying,
    salt            text,
    email           varchar(100),
    nom             varchar(100)             not null,
    prenom          varchar(100)
        constraint utilisateur_pk
            unique,
    tel             varchar(20),
    "urlMotDePasse" varchar(100),
    "createdAt"     timestamp with time zone not null,
    "updatedAt"     timestamp with time zone not null
);

alter table utilisateur
    owner to postgres;

create table role_droit
(
    role_id     integer                  not null
        constraint undefined_role_droit_role_id_fkey
            references role,
    droit_id    integer                  not null
        constraint undefined_role_droit_droit_id_fkey
            references droit,
    "createdAt" timestamp with time zone not null,
    "updatedAt" timestamp with time zone not null
);

alter table role_droit
    owner to postgres;

create table utilisateur_role
(
    utilisateur_id integer                  not null
        constraint undefined_utilisateur_role_utilisateur_id_fkey
            references utilisateur,
    role_id        integer                  not null
        constraint undefined_utilisateur_role_role_id_fkey
            references role,
    "createdAt"    timestamp with time zone not null,
    "updatedAt"    timestamp with time zone not null
);

alter table utilisateur_role
    owner to postgres;

create table clock
(
    id          serial
        constraint clock_pk
            primary key,
    "clockIn"   timestamp default now() not null,
    "clockOut"  timestamp,
    user_id     integer                 not null
        constraint clock_utilisateur_id_fk
            references utilisateur,
    "createdAt" timestamp default now() not null,
    "updatedAt" timestamp default now() not null
);

alter table clock
    owner to postgres;

create view vue_utilisateur_role_droit
            ("userId", email, "roleId", "roleLibelle", "droitId", "droitLibelle", "droitCode") as
SELECT utilisateur.id AS "userId",
       utilisateur.email,
       role.id        AS "roleId",
       role.libelle   AS "roleLibelle",
       droit.id       AS "droitId",
       droit.libelle  AS "droitLibelle",
       droit.code     AS "droitCode"
FROM utilisateur_role
         JOIN utilisateur ON utilisateur_role.utilisateur_id = utilisateur.id
         JOIN role ON utilisateur_role.role_id = role.id AND role.etat::text = '1'::text
         JOIN role_droit ON role_droit.role_id = utilisateur_role.role_id
         JOIN droit ON role_droit.droit_id = droit.id AND droit.etat::text = '1'::text;

alter table vue_utilisateur_role_droit
    owner to postgres;

INSERT INTO public.droit (id, etat, libelle, code, "createdAt", "updatedAt") VALUES (2, '1', 'clock', 'clock', '2024-02-24 07:41:12.276000 +00:00', '2024-02-24 07:43:51.423000 +00:00');
INSERT INTO public.droit (id, etat, libelle, code, "createdAt", "updatedAt") VALUES (3, '1', 'liste des utilisateurs', 'listUser', '2024-02-24 07:41:50.616000 +00:00', '2024-02-24 07:43:52.558000 +00:00');
INSERT INTO public.droit (id, etat, libelle, code, "createdAt", "updatedAt") VALUES (4, '1', 'création utilisateur', 'createUser', '2024-02-24 07:42:20.465000 +00:00', '2024-02-24 07:43:56.493000 +00:00');
INSERT INTO public.droit (id, etat, libelle, code, "createdAt", "updatedAt") VALUES (5, '1', 'modification utilisateur', 'updateUser', '2024-02-24 07:43:10.100000 +00:00', '2024-02-24 07:43:57.211000 +00:00');
INSERT INTO public.droit (id, etat, libelle, code, "createdAt", "updatedAt") VALUES (6, '1', 'historique clock', 'historiqueClock', '2024-02-24 07:43:11.903000 +00:00', '2024-02-24 07:43:57.918000 +00:00');
INSERT INTO public.droit (id, etat, libelle, code, "createdAt", "updatedAt") VALUES (7, '1', 'rectification clock', 'rectificationClock', '2024-02-24 07:43:33.291000 +00:00', '2024-02-24 07:43:58.591000 +00:00');

INSERT INTO public.role (id, etat, libelle, description, "createdAt", "updatedAt") VALUES (5, '1', 'Utilisateur', 'user', '2024-02-24 12:19:36.888000 +00:00', '2024-02-24 12:19:42.806000 +00:00');
INSERT INTO public.role (id, etat, libelle, description, "createdAt", "updatedAt") VALUES (6, '1', 'Admin', 'admin', '2024-02-24 12:20:02.855000 +00:00', '2024-02-24 12:20:04.092000 +00:00');

INSERT INTO public.utilisateur (id, "motDePasse", etat, salt, email, nom, prenom, tel, "urlMotDePasse", "createdAt", "updatedAt") VALUES (1, '$2b$04$2G172LGE8x0fstVviYPAYuWWApWcLh7hyaD77ma9Y5mE3rjDY4So.', '0', '$2b$04$2G172LGE8x0fstVviYPAYu', 'bixlemac@gmail.com', 'Billy', 'Billy', '(+261)324567890', null, '2024-02-23 21:56:05.626000 +00:00', '2024-02-24 19:47:51.933000 +00:00');

INSERT INTO public.role_droit (role_id, droit_id, "createdAt", "updatedAt") VALUES (6, 2, '2024-02-24 07:46:19.356000 +00:00', '2024-02-24 07:46:23.438000 +00:00');
INSERT INTO public.role_droit (role_id, droit_id, "createdAt", "updatedAt") VALUES (6, 3, '2024-02-24 07:46:20.203000 +00:00', '2024-02-24 07:46:24.112000 +00:00');
INSERT INTO public.role_droit (role_id, droit_id, "createdAt", "updatedAt") VALUES (6, 4, '2024-02-24 07:46:20.849000 +00:00', '2024-02-24 07:46:24.681000 +00:00');
INSERT INTO public.role_droit (role_id, droit_id, "createdAt", "updatedAt") VALUES (6, 5, '2024-02-24 07:46:21.439000 +00:00', '2024-02-24 07:46:25.211000 +00:00');
INSERT INTO public.role_droit (role_id, droit_id, "createdAt", "updatedAt") VALUES (6, 6, '2024-02-24 07:46:21.999000 +00:00', '2024-02-24 07:46:25.777000 +00:00');
INSERT INTO public.role_droit (role_id, droit_id, "createdAt", "updatedAt") VALUES (6, 7, '2024-02-24 07:46:22.542000 +00:00', '2024-02-24 07:46:26.296000 +00:00');
INSERT INTO public.role_droit (role_id, droit_id, "createdAt", "updatedAt") VALUES (5, 2, '2024-02-24 21:09:29.670000 +00:00', '2024-02-24 21:09:31.301000 +00:00');

INSERT INTO public.utilisateur_role (utilisateur_id, role_id, "createdAt", "updatedAt") VALUES (1, 6, '2024-02-24 07:47:02.069000 +00:00', '2024-02-24 09:41:19.593000 +00:00');

alter table clock
    add launch boolean default false not null;

alter table utilisateur
    add exceptionlunch boolean default false not null;


